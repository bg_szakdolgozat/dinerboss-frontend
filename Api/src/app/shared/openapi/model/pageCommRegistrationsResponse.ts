/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { PageCommRegistrationsResponseAllOf } from './pageCommRegistrationsResponseAllOf';
import { CommodityRegistrationDTO } from './commodityRegistrationDTO';
import { ResponseBase } from './responseBase';


export interface PageCommRegistrationsResponse { 
    rc: number;
    data: Array<CommodityRegistrationDTO>;
    totalCount: number;
}

