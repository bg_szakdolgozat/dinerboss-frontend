/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { AdminProfile } from './adminProfile';
import { LoginAdminResponseAllOf } from './loginAdminResponseAllOf';
import { ResponseBase } from './responseBase';


export interface LoginAdminResponse { 
    rc: number;
    admin?: AdminProfile;
}

