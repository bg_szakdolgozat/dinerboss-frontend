/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { PageValueSearch } from './pageValueSearch';
import { PageStoreSearchAllOf } from './pageStoreSearchAllOf';


export interface PageStoreSearch { 
    value?: string;
    name?: string;
    compid?: number;
    compIds?: Array<number>;
    storeIds?: Array<number>;
}

