/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface CreateReportDTO { 
    customerId: number;
    storeId: number;
    income?: number;
    expense?: number;
    comment?: string;
    date?: string;
}

