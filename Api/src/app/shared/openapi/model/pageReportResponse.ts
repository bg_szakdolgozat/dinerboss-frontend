/**
 * Api Documentation
 * Api Documentation
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { PageReportResponseAllOf } from './pageReportResponseAllOf';
import { ReportDTO } from './reportDTO';
import { ResponseBase } from './responseBase';


export interface PageReportResponse { 
    rc: number;
    data: Array<ReportDTO>;
    totalCount: number;
}

