import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { CheckoutHistory } from './checkouthistory.model';

export interface CheckoutHistoryState extends EntityState<CheckoutHistory> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'checkouthistory' })
export class CheckoutHistoryStore extends EntityStore<CheckoutHistoryState> {

    constructor() {
        super();
    }

}
