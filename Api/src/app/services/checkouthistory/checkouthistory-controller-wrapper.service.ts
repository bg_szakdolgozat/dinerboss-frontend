import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { CheckoutControllerService, CommodityControllerService, PageCheckoutResponse, PageCheckoutSearch, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsResponse, PageCommRegistrationsSearch, PageCompaniesResponse, PageCompaniesSearch, PageData, PageTransmissionSearch, PageTransmissionsResponse, StorageControllerService } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { CheckoutHistory, toCheckoutHistories } from './checkouthistory.model';

export class CheckoutHistoryControllerWrapperService extends BasePagingService<CheckoutHistory, PageTransmissionSearch> {

    constructor(
        private companyController: CheckoutControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageTransmissionSearch>): Observable<BasePageableResponse<CheckoutHistory>> {
        return this.companyController.pageTransmissions({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageTransmissionsResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toCheckoutHistories(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
