import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { AddTransmissionDTO, AddTransmissionResponse, CheckoutControllerService, CommodityControllerService, CommodityDTO, CreateCheckoutDTO, CreateCheckoutResponse, CreateCommodityDTO, CreateCommodityResponse, DeleteCheckoutResponse, DeleteCommodityRequest, DeleteCommodityResponse, ModifyCheckoutDTO, ModifyCheckoutResponse, ModifyCommodityResponse, PageCheckoutResponse, PageCheckoutSearch, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsSearch, PageTransmissionSearch, StorageControllerService } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { CheckoutHistoryControllerWrapperService } from './checkouthistory-controller-wrapper.service';
import { CheckoutHistory } from './checkouthistory.model';
import { CheckoutHistoryQuery } from './checkouthistory.query';
import { CheckoutHistoryState, CheckoutHistoryStore } from './checkouthistory.store';

@Injectable({ providedIn: 'root' })
export class CheckoutHistoryService extends BasePageableAkitaService<CheckoutHistory, CheckoutHistoryState, PageTransmissionSearch>{

    constructor(
        protected store: CheckoutHistoryStore,
        protected query: CheckoutHistoryQuery,
        private controller: CheckoutControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new CheckoutHistoryControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }

}
