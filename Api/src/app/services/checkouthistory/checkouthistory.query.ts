import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { CheckoutHistoryState, CheckoutHistoryStore } from './checkouthistory.store';

@Injectable({ providedIn: 'root' })
export class CheckoutHistoryQuery extends QueryEntity<CheckoutHistoryState> {

    constructor(protected store: CheckoutHistoryStore) {
        super(store);
    }

}
