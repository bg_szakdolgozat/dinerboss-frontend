import { CheckoutDTO, CustomerProfileDTO, StoreDTO, TransmissionDTO } from 'src/app/shared/openapi';
import { Identifiable } from '../common/base-entity';
import { CheckoutHistoryState } from './checkouthistory.store';

export interface CheckoutHistory extends Identifiable<CheckoutHistoryState> {
    id?: number;
    date?: Date;
    comment?: string;
    invalue: boolean;
    customer?: CustomerProfileDTO;
    checkout?: CheckoutDTO;
    money?: number;
}

export function toCheckoutHistories(dtos: TransmissionDTO[]): CheckoutHistory[] {
    return dtos.map(dto => toCheckoutHistory(dto));
}

export function toCheckoutHistory(dto: TransmissionDTO): CheckoutHistory {
    return {
        id: dto.id,
        date: dto.date,
        comment: dto.comment,
        invalue: dto.invalue,
        customer: dto.customer,
        checkout: dto.checkout,
        money: dto.money
    } as CheckoutHistory;
}
