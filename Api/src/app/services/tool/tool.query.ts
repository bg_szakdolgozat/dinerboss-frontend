import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ToolState, ToolStore } from './tool.store';

@Injectable({ providedIn: 'root' })
export class ToolQuery extends QueryEntity<ToolState> {

    constructor(protected store: ToolStore) {
        super(store);
    }

}
