import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Tool } from './tool.model';

export interface ToolState extends EntityState<Tool> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'company' })
export class ToolStore extends EntityStore<ToolState> {

    constructor() {
        super();
    }

}
