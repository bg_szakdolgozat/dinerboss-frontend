import { CommodityDTO } from 'src/app/shared/openapi';
import { Identifiable } from '../common/base-entity';
import { ToolState } from './tool.store';

export interface Tool extends Identifiable<ToolState> {
    id?: number;
    name: string;
    purchasePrice?: number;
    unit?: string;
    warranty?: number;
    compId?: number;
}

export function toCommodities(organizationsDTO: CommodityDTO[]): Tool[] {
    return organizationsDTO.map(dto => toCommodity(dto));
}

export function toCommodity(commodityDTO: CommodityDTO): Tool {
    return {
        id: commodityDTO.id,
        name: commodityDTO.name,
        purchasePrice: commodityDTO.purchasePrice,
        unit: commodityDTO.unit,
        warranty: commodityDTO.warranty,
        compId: commodityDTO.compId
    } as Tool;
}
