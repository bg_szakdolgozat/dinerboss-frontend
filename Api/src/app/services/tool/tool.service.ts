import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { PageCommoditySearch, CommodityControllerService, CreateCommodityDTO, CreateCommodityResponse, DeleteCommodityResponse, PageCommodityResponse } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { Commodity, toCommodity } from '../commodities/commodity.model';
import { CommodityQuery } from '../commodities/commodity.query';
import { CommodityState, CommodityStore } from '../commodities/commodity.store';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { ToolControllerWrapperService } from './tool-controller-wrapper.service';
import { Tool } from './tool.model';
import { ToolQuery } from './tool.query';
import { ToolState, ToolStore } from './tool.store';

@Injectable({ providedIn: 'root' })
export class ToolService extends BasePageableAkitaService<Tool, ToolState, PageCommoditySearch>{

    constructor(
        protected store: ToolStore,
        protected query: ToolQuery,
        private controller: CommodityControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new ToolControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }

    create(dto: CreateCommodityDTO): Observable<CreateCommodityResponse> {
        return this.controller.createTool({ commodity: dto }).pipe(
            map((response: CreateCommodityResponse) => {
                return response;
            })
        );
    }
    delete(id: number): Observable<DeleteCommodityResponse> {
        return this.controller.deleteTool({ id }).pipe(
            map((response: DeleteCommodityResponse) => {
                return response;
            })
        );
    }
    getById(id: string): Tool {
        return this.query.getEntity(id);
    }
    refreshAll(): Observable<void> {
        return this.controller.pageTools({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
            }
        }).pipe(
            map((response: PageCommodityResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCommodity(dto));
                    } else {
                        this.store.add(toCommodity(dto));
                    }
                }
            })
        );
    }
    refreshToolsByCompIds(compIds: number[]): Observable<void> {
        return this.controller.pageTools({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageCommodityResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCommodity(dto));
                    } else {
                        this.store.add(toCommodity(dto));
                    }
                }
            })
        );
    }
}
