import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CommodityControllerService, PageCommodityResponse, PageCommoditySearch, PageCompaniesResponse, PageCompaniesSearch, PageData } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { toCommodities, Tool } from './tool.model';

export class ToolControllerWrapperService extends BasePagingService<Tool, PageCommoditySearch> {

    constructor(
        private companyController: CommodityControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageCommoditySearch>): Observable<BasePageableResponse<Tool>> {
        return this.companyController.pageTools({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageCommodityResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toCommodities(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
