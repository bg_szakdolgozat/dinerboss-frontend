import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CustomerControllerService, PageCustomersResponse, PageCustomersSearch, PageData } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { CustomerProfileDO, toCustomers } from './customer.model';

export class CustomerControllerWrapperService extends BasePagingService<CustomerProfileDO, PageCustomersSearch> {


    constructor(
        private companyController: CustomerControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageCustomersSearch>): Observable<BasePageableResponse<CustomerProfileDO>> {
        return this.companyController.pageCustomers({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageCustomersResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toCustomers(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
