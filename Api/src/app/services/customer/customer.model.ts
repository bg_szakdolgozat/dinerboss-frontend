// tslint:disable-next-line:quotemark
import { CustomerProfileDTO } from "src/app/shared/openapi";
import { Identifiable } from '../common/base-entity';
import { Company } from '../company/company.model';
import { CustomerState } from './customer.store';

export interface CustomerProfileDO extends Identifiable<CustomerState> {
    id: number;
    title: string;
    firstName: string;
    lastName: string;
    status: number;
    phoneNumber: string;
    email: string;
    companies: Array<Company>;

}
export function toCustomers(organizationsDTO: CustomerProfileDTO[]): CustomerProfileDO[] {
    return organizationsDTO.map(dto => toCustomer(dto));
}

export function toCustomer(customerProfileDTO: CustomerProfileDTO): CustomerProfileDO {
    return {
        id: customerProfileDTO.id,
        title: customerProfileDTO.title,
        firstName: customerProfileDTO.firstName,
        lastName: customerProfileDTO.lastName,
        status: customerProfileDTO.status,
        phoneNumber: customerProfileDTO.phoneNumber,
        email: customerProfileDTO.email,
        companies: customerProfileDTO.companies
    } as CustomerProfileDO;
}
