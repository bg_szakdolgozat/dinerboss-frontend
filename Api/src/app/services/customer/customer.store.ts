import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { CustomerProfileDO } from './customer.model';

export interface CustomerState extends EntityState<CustomerProfileDO> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'customer' })
export class CustomerStore extends EntityStore<CustomerState> {

    constructor() {
        super();
    }

}
