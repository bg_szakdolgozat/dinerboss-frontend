import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { PageCustomersSearch, CustomerControllerService, CreateCustomerDTO, CreateCustomerResponse, ModifyCustomerResponse, ModifyCustomerRequest, CustomerProfileDTO, LoginCustomerRequest, LoginCustomerResponse, PageCustomersResponse, GetCustomerByEmailRequest, GetCustomerByEmailResponse, DeleteCustomerResponse } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { CustomerControllerWrapperService } from './customer-controller-wrapper.service';
import { CustomerProfileDO, toCustomer } from './customer.model';
import { CustomerQuery } from './customer.query';
import { CustomerState, CustomerStore } from './customer.store';

@Injectable({ providedIn: 'root' })
export class CustomerService extends BasePageableAkitaService<CustomerProfileDO, CustomerState, PageCustomersSearch>{

    constructor(
        protected store: CustomerStore,
        protected query: CustomerQuery,
        private controller: CustomerControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new CustomerControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }

    create(dto: CreateCustomerDTO): Observable<CreateCustomerResponse> {
        return this.controller.createCustomer({ customer: dto }).pipe(
            map((response: CreateCustomerResponse) => {
                return response;
            })
        );
    }
    modify(dto: CustomerProfileDTO): Observable<ModifyCustomerResponse> {
        return this.controller.modifyCustomer({ customer: dto }).pipe(
            map((response: ModifyCustomerResponse) => {
                return response;
            })
        );
    }
    getByEmail(email: string): Observable<GetCustomerByEmailResponse> {
        return this.controller.getCustomerByEmail({ email }).pipe(
            map((response: GetCustomerByEmailResponse) => {
                return response;
            })
        );
    }
    delete(id: number): Observable<DeleteCustomerResponse> {
        return this.controller.deleteCustomer({ id }).pipe(
            map((response: DeleteCustomerResponse) => {
                return response;
            })
        );
    }
    login(dto: LoginCustomerRequest): Observable<LoginCustomerResponse> {
        return this.controller.loginCustomer(dto).pipe(
            map((response: LoginCustomerResponse) => {
                return response;
            })
        );
    }
    getById(id: string): CustomerProfileDO {
        return this.query.getEntity(id);
    }
    refreshAll(): Observable<void> {
        return this.controller.pageCustomers({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
            }
        }).pipe(
            map((response: PageCustomersResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCustomer(dto));
                    } else {
                        this.store.add(toCustomer(dto));
                    }
                }
            })
        );
    }
    getByStoreIds(storeIds): Observable<PageCustomersResponse> {
        return this.controller.pageCustomers({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                storeIds
            }
        });
    }
}
