import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notification: MatSnackBar) { }

  success() {
    const message = 'Siker';
    this.successMessage(message);
  }

  successMessage(message: string, duration = 3000) {
    this.notification.open(message, null, {
      duration,
      verticalPosition: 'bottom',
      panelClass: 'success-dialog'
    });
  }

  error(message, duration?) {
    this.notification.open(message, null, {
      duration: duration || 4000,
      verticalPosition: 'bottom',
      panelClass: 'error-dialog'
    });
  }


}
