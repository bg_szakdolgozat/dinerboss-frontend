import * as moment from 'moment';
import { formatDate } from '@angular/common';
import { environment } from 'src/environments/environment';

const DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ssZ';
export class DateUtil {

  public static stringToDate(date: string): Date {
    const currentLanguage = localStorage.getItem('locale') || environment.defaultLanguage;
    moment.locale(currentLanguage);
    return moment.utc(date, DATE_FORMAT).toDate();
  }

  public static numberToDate(date: number): Date {
    const currentLanguage = localStorage.getItem('locale') || environment.defaultLanguage;
    moment.locale(currentLanguage);
    return moment.utc(date).toDate();
  }

  public static dateToString(date: Date): string {
    return date.toISOString();
  }

  public static utcToDate(date: Date): Date {
    const currentLanguage = localStorage.getItem('locale') || environment.defaultLanguage;
    moment.locale(currentLanguage);
    return moment.utc(date).toDate();
  }

  public static dateToMoment(date: Date) {
    const currentLanguage = localStorage.getItem('locale') || environment.defaultLanguage;
    moment.locale(currentLanguage);
    return moment.utc(date);
  }

  public static truncateDate(date: Date) {
    date.setMilliseconds(0);
    date.setSeconds(0);
    date.setMinutes(0);
    date.setHours(0);
    return date;
  }

  public static maximizeDate(date: Date) {
    date.setSeconds(59);
    date.setMinutes(59);
    date.setHours(23);
    return date;
  }

  public static getDate(date) {
    if (date) {
      const currentLanguage = localStorage.getItem('locale') || environment.defaultLanguage;
      if (currentLanguage === 'hu') {
        return formatDate(date, 'yyyy.MM.dd', 'en');
      } else {
        return formatDate(date, 'dd/MM/yyyy', 'en');
      }
    }
  }
  public static getFullDate(date) {
    if (date) {
      const currentLanguage = localStorage.getItem('locale') || environment.defaultLanguage;
      if (currentLanguage === 'hu') {
        return formatDate(date, 'yyyy.MM.dd  HH:mm', 'en');
      } else {
        return formatDate(date, 'dd/MM/yyyy  HH:mm', 'en');
      }
    }
  }
  public static getFullDateFromUtc(date) {
    if (date) {
      return this.getFullDate(this.utcToDate(date));
    }
  }
  public static getDateWithMonth(date) {
    if (date) {
      const currentLanguage = localStorage.getItem('locale') || environment.defaultLanguage;
      if (currentLanguage === 'hu') {
        return formatDate(date, 'yyyy.MM', 'en');
      } else {
        return formatDate(date, 'MM/yyyy', 'en');
      }
    }
  }

}
