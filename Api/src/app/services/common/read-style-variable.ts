const PREFIX = '--';

export class ReadStyleVariable {

  public static readValue(propertyName: string): string {
    const bodyStyles = window.getComputedStyle(document.body);
    const resultValue = bodyStyles.getPropertyValue(`${PREFIX}${propertyName}`);
    return resultValue ? resultValue.trim() : '';
  }

}
