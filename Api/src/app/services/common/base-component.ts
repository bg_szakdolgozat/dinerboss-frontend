import { Directive, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { BreadcrumbItem } from 'src/app/common-base/breadcrumb/breadcrumb.component';
@Directive()
// tslint:disable-next-line:directive-class-suffix
export class BaseComponent implements OnDestroy {
  public breadcrumbItemList: BreadcrumbItem[];
  public subManager = new Subscription();

  ngOnDestroy(): void {
    this.subManager.unsubscribe();
  }
}
