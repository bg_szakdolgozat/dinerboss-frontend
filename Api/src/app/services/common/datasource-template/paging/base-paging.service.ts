import { Observable } from 'rxjs';
import { BasePageableRequest, BasePageableResponse } from './page';

export abstract class BasePagingService<T, F> {
  abstract getPageableData(query: BasePageableRequest<F>): Observable<BasePageableResponse<T>>;
}
