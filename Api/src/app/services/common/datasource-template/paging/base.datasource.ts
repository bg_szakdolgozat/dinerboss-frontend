import { EntityState, EntityStore, QueryEntity } from '@datorama/akita';
import { AkitaMatDataSource } from 'akita-filters-plugin/datasource';
import { BehaviorSubject, Observable } from 'rxjs';
import { Identifiable } from '../../base-entity';
import { LoaderService } from '../../loader.service';
import { BasePagingService } from './base-paging.service';
import { BasePageableRequest } from './page';

export class BaseDataSource<T extends Identifiable<S>, S extends EntityState<T>, F> extends AkitaMatDataSource<T, S> {
  protected subject = new BehaviorSubject<T[]>([]);
  public loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  public data: T[] = [];
  public totalCount = 0;

  constructor(
    protected query: QueryEntity<S>,
    protected store: EntityStore<S>,
    private basePagingService: BasePagingService<T, F>,
    private loadService: LoaderService
  ) {
    super(query);
  }

  loadData(request: BasePageableRequest<F>) {
    this.loadingSubject.next(true);
    this.loadService.load(true);
    this.basePagingService.getPageableData(request).subscribe(
      response => {
        this.totalCount = response.totalCount;
        this._updatePaginator(this.totalCount);
        this.data = response.data;
        for (const entity of response.data) {
          const exist = this.query.getEntity(entity.id);
          if (exist) {
            this.store.update(exist.id, entity as any);
          } else {
            this.store.add(entity as any);
          }
        }
        this.loadingSubject.next(false);
        this.loadService.load(false);
        const retrievedData = this.data.map(e => this.query.getEntity(e.id));
        this.subject.next(retrievedData);
      }, () => {
        this.loadingSubject.next(false);
        this.loadService.load(false);
      });
  }

  connect(): Observable<any> {
    return this.subject.asObservable();
  }
}
