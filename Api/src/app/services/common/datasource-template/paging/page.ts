import { BaseResponse } from '../../base';
import { PageData } from 'src/app/shared/openapi';

export interface BasePageableRequest<T> {
  params?: T;
  page: PageableBean;
}

export interface BasePageableResponse<T> extends BaseResponse {
  data: T[];
  totalCount: number;
}

export interface PageableBean {
  sortOrder?: SortOrder;
  pageNumber: number;
  pageSize: number;
}

export interface SortOrder {
  orderBy: string;
  direction?: string;
}
