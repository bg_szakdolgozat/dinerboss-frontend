import { AfterViewInit, HostListener, OnInit, ViewChild, Directive } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { Router } from '@angular/router';
import { EntityState } from '@datorama/akita';
import { merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { BaseComponent } from '../base-component';
import { Identifiable } from '../base-entity';
import { BasePageableAkitaService } from './akita/akita.service';
import { BaseDataSource } from './paging/base.datasource';
import { BasePageableRequest } from './paging/page';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class BaseList<T extends Identifiable<S>, S extends EntityState<T>, F>
  extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public entity: T;
  public entities: T[] = [];
  // tslint:disable-next-line:member-ordering
  public dataSource: BaseDataSource<T, S, F>;
  public displayedColumns: string[] = this.initColumns();

  public isResponse = false;

  abstract initColumns(): string[];
  abstract initColumnsOnDevice(): string[];
  abstract initFilter(): void;
  abstract initDataSource(): void;
  abstract searchParams(params?): F;


  constructor(
    private basePageableAkitaService: BasePageableAkitaService<T, S, F>,
    private routers?: Router
  ) {
    super();
  }

  ngOnInit() {
    this.changeLocale();
    if (window.innerWidth < 500) {
      this.isResponse = true;
      this.displayedColumns = this.initColumnsOnDevice();
    }
    this.newInstanceDataSource();
    this.dataSource = this.basePageableAkitaService.getDataSource();
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.paginator.pageSizeOptions = [10, 20, 50, 100, 200];
    this.initDataSource();
    this.clearSelection();
    this.initFilter();
    if (this.routers) {
      const sub = this.dataSource.loading$.subscribe(() => {
        if (this.dataSource.totalCount === 0 && this.paginator.hasPreviousPage()) {
          this.paginator.pageIndex = 0;
          this.loadDataSource();
        }
      });
      this.subManager.add(sub);
    }
  }

  ngAfterViewInit(): void {
    const sortSubscription = this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    const mergedSubscription = merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadDataSource())
      ).subscribe();
    this.subManager
      .add(sortSubscription)
      .add(mergedSubscription);
  }

  public loadDataSource(params?): void {
    this.dataSource.loadData(this.defaultPage(params));
  }

  getStoredPage(): BasePageableRequest<F> {
    if (this.routers) {
      let key = this.routers.url;
      if (key.startsWith('/')) {
        key = this.routers.url.substring(1);
      }
      const storedPageAsString = sessionStorage.getItem(key);
      if (storedPageAsString) {
        return JSON.parse(storedPageAsString) as BasePageableRequest<F>;
      }
    }
    return undefined;
  }

  restoreStoredPage() {
    const page: BasePageableRequest<F> = this.getStoredPage();
    if (page && page.page) {
      if (page.page.sortOrder) {
        this.sort.active = page.page.sortOrder.orderBy ? page.page.sortOrder.orderBy : undefined;
        this.sort.direction = page.page.sortOrder.direction ? page.page.sortOrder.direction.toLowerCase() as SortDirection : undefined;
      }
      this.paginator.pageIndex = page.page.pageNumber;
      this.paginator.pageSize = page.page.pageSize;
    }
  }

  savePage() {
    if (this.routers) {
      let key = this.routers.url;
      if (key.startsWith('/')) {
        key = this.routers.url.substring(1);
      }
      sessionStorage.setItem(key, JSON.stringify(this.defaultPage()));
    }
  }

  removeStoredPage() {
    if (this.routers) {
      let key = this.routers.url;
      if (key.startsWith('/')) {
        key = this.routers.url.substring(1);
      }
      sessionStorage.removeItem(key);
    }
  }

  defaultPage(params?): BasePageableRequest<F> {
    if (params) {
      return {
        params,
        page: {
          sortOrder: {
            orderBy: this.sort.active,
            direction: this.sort.direction.toUpperCase()
          },
          pageNumber: this.paginator.pageIndex,
          pageSize: this.paginator.pageSize
        }
      } as BasePageableRequest<F>;

    } else {
      return {
        params: this.searchParams(),
        page: {
          sortOrder: {
            orderBy: this.sort.active,
            direction: this.sort.direction.toUpperCase()
          },
          pageNumber: this.paginator.pageIndex,
          pageSize: this.paginator.pageSize
        }
      } as BasePageableRequest<F>;
    }
  }

  isSelected(row: T) {
    return this.basePageableAkitaService.isActive(row.id);
  }

  isThisSelected(row: T) {
    return this.basePageableAkitaService.isActive(row.id);
  }

  hasValue() {
    return this.basePageableAkitaService.getActives().length > 0;
  }

  getSelected() {
    return this.basePageableAkitaService.getActives();
  }

  getLengthOfSelected() {
    return this.basePageableAkitaService.getActives().length;
  }

  onRowClicked(row: T) {
    this.clearSelection();
    this.basePageableAkitaService.setActive(row.id);
    this.entity = row;
    this.entities = [this.entity];
  }

  onToggleSelect(row: T) {
    if (this.isSelected(row)) {
      this.basePageableAkitaService.removeActive(row.id);
      this.entities = this.entities.filter(e => e.id !== row.id);
    } else {
      this.basePageableAkitaService.addActive(row.id);
      this.entities.push(row);
    }
    if (this.getLengthOfSelected() === 1) {
      this.entity = row;
    }
  }

  isAllSelected() {
    const lengthOfCurrentPage = this.dataSource.data.length;
    let i = 0;
    for (const row of this.dataSource.data) {
      if (this.isSelected(row)) {
        i++;
      }
    }
    return lengthOfCurrentPage === i;
  }



  masterToggle() {
    this.isAllSelected() ? this.clearSelection() : this.masterSelection();
  }

  masterSelection() {
    this.dataSource.data.forEach(element => {
      if (!this.isSelected(element)) {
        this.basePageableAkitaService.addActive(element.id);
      }
    });
  }

  clearSelection() {
    this.basePageableAkitaService.removeAllActive();
  }

  searchControlInit(control: AbstractControl, length?: number) {
    // tslint:disable-next-line:no-bitwise
    length = length | 0;
    const searchControlSub = control.valueChanges.pipe(
      debounceTime(250),
      distinctUntilChanged()
    ).subscribe(() => {
      // if (control.value && (control.value.length === 0 || control.value.length >= length)) {
      this.clearSelection();
      this.loadDataSource();
      // }
    });
    this.subManager.add(searchControlSub);
  }

  newInstanceDataSource() {
    // this.dataSource = this.basePageableAkitaService.getDataSource();
  }

  changeLocale() {

  }

  @HostListener('window:resize', ['$event'])
  // tslint:disable-next-line:typedef
  onResize(event) {
    // tslint:disable-next-line:no-unused-expression
    if (event.target.innerWidth < 500) {
      this.isResponse = true;
      this.displayedColumns = this.initColumnsOnDevice();
    } else {
      this.isResponse = false;
      this.displayedColumns = this.initColumns();
    }
  }

}

