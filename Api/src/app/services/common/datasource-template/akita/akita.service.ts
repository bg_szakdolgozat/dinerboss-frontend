import { BaseDataSource } from '../paging/base.datasource';
import { Identifiable } from '../../base-entity';
import { EntityState, QueryEntity, EntityStore } from '@datorama/akita';
import { BasePagingService } from '../paging/base-paging.service';
import { BasePageableRequest } from '../paging/page';
import { LoaderService } from '../../loader.service';

export class BasePageableAkitaService<T extends Identifiable<S>, S extends EntityState<T>, F> {
  datasource: BaseDataSource<T, S, F>;
  constructor(
    protected query: QueryEntity<S>,
    protected store: EntityStore<S>,
    private basePagingService: BasePagingService<T, F>,
    private loaderService: LoaderService
  ) {
    this.datasource = new BaseDataSource(this.query, this.store, this.basePagingService, this.loaderService);
  }

  getDataSource(): BaseDataSource<T, S, F> {
    return this.datasource;
  }

  loadDataSource(params: BasePageableRequest<F>) {
    this.datasource.loadData(params);
  }

  setActive(id) {
    if (!this.isActive(id)) {
      this.store.setActive(id);
    }
  }

  addActive(id) {
    if (!this.isActive(id)) {
      const actives: any = [];
      for (const iterator of this.getActives()) {
        actives.push(iterator.id);
      }
      actives.push(id);
      this.store.setActive(actives);
    }
  }

  removeActive(id) {
    if (this.isActive(id)) {
      const actives: any = this.getActives().filter(a => a.id !== id);
      this.setActive(null);
      for (const element of actives) {
        this.addActive(element.id);
      }
    }
  }

  removeAllActive() {
    this.setActive(null);
  }

  isActive(id) {
    return this.getActives().some(a => a.id === id);
  }

  getActives(): T[] {
    let array = [];
    array = array.concat(this.query.getActive());
    return array.filter(a => a);
  }

  getAll(): T[] {
    return this.query.getAll();
  }
}

export function uniqueArray(array) {
  const a = array.concat();
  for (let i = 0; i < a.length; ++i) {
    for (let j = i + 1; j < a.length; ++j) {
      if (a[i] === a[j]) {
        a.splice(j--, 1);
      }
    }
  }
  return a;
}
