import { environment } from 'src/environments/environment';

export class LocalizeNameManager {
  constructor() { }
  // tslint:disable-next-line:typedef
  public static localizeName(member) {
    const lang = localStorage.getItem('locale') || environment.defaultLanguage;
    const firstName = member.firstName || '';
    const lastName = member.lastName || '';
    const title = member.title || '';
    if (lang === 'hu') {
      return `${title} ${lastName} ${firstName}`;
    } else {
      return `${title} ${firstName} ${lastName}`;
    }
  }
}

