import { getIDType } from '@datorama/akita';

export interface Identifiable<S> {
  id?: getIDType<S>;
}

export interface Nameable {
  name?: string;
  email?: string;
}

export enum UiLanguage {
  EN,
  HU
}
