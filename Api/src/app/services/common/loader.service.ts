import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private loader = new BehaviorSubject<boolean>(false);
  constructor() { }

  load(load: boolean) {
    this.loader.next(load);
  }

  isLoad() {
    return this.loader.pipe();
  }
}
