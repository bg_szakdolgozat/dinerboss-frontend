import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Optional } from '@angular/core';
import { SkipSelf } from '@angular/core';
import { ReportGuard } from 'src/app/pages/report/report.guard';
import { AdminGuard } from 'src/app/pages/admin/admin.guard';
import { SuperadminGuard } from 'src/app/pages/superadmin/superadmin.guard';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers:
    [
      SuperadminGuard,
      ReportGuard,
      AdminGuard
    ]
})
export class DomainModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainModule) {
    console.log('http domain module');
    if (parentModule) {
      throw new Error(
        'DomainModule is already loaded. Import it in the AppModule only');
    }
  }
}
