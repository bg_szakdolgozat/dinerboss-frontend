import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GetLeagueByIdRequest, LeagueDetailsDTO, LeagueDTO } from './league';

@Injectable()
export abstract class LeagueController{
  public abstract getLeagues(): Observable<LeagueDTO[]>;
  public abstract getLeagueById(request: GetLeagueByIdRequest): Observable<LeagueDetailsDTO>;
}
