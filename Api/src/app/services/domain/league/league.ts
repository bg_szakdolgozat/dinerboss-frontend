export interface GetLeaguesResponse {
    Leagues: LeagueDTO[];
}

export interface LeagueDTO{
    id: number;
    name: string;
}

export interface LeagueDetailsDTO{
    id: number;
    name: string;
}

export interface GetLeagueByIdRequest {
    id: number;
}
