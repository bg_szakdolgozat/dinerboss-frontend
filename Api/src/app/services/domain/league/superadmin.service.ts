import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AdminControllerService, GetAdminResponse, LoginAdminRequest, LoginAdminResponse } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SuperadminService {
  authenticated = false;
  constructor(protected controller: AdminControllerService, private http: HttpClient) {
    controller.configuration.basePath = `${environment.apiBaseUrl}`;

  }

  getById(email: string): Observable<GetAdminResponse> {
    return this.controller.getAdminByEmail({ email }).pipe(map((response: GetAdminResponse) => {
      return response ? (response) : null;
    }));
  }

  login(adminEmail: string, password: string): Observable<LoginAdminResponse> {
    return this.controller.loginSuperadmin({ adminEmail, password }).pipe(
      map((response: LoginAdminResponse) => {
        return response;
      })
    );
  }


}
