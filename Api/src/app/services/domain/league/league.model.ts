import { LeagueDetailsDTO, LeagueDTO } from './league';

export interface League {
    id: number;
    name: string;
}

export interface LeagueDetails {
    id: number;
    name: string;
}

export interface GetLeagueById {
    id: number;
}

// tslint:disable-next-line: typedef
export function toLeague(leagueDTO: LeagueDTO): League {
    return {
        id: leagueDTO.id,
        name: leagueDTO.name
    };
}
// tslint:disable-next-line: typedef
export function toLeagues(getLeaguesResponse: LeagueDTO[]): League[] {
    return getLeaguesResponse.map(dto => toLeague(dto));
}

export function toLeagueResponse(leagueDetailsDTO: LeagueDetailsDTO): League {
    return {
        id: leagueDetailsDTO.id,
        name: leagueDetailsDTO.name
    };
}
