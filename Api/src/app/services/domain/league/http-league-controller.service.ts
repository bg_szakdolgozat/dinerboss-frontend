import { HttpClient, HttpHeaders, HttpParameterCodec } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { GetLeagueByIdRequest, LeagueDetailsDTO, LeagueDTO } from './league';
import { LeagueController } from './league-controller.service';

@Injectable()
export class HttpLeagueController implements LeagueController {
    private readonly BASE_URL = `${environment.apiBaseUrl}/admin`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }



    getLeagues(): Observable<LeagueDTO[]> {
        return this.httpClient.get<LeagueDTO[]>(`${this.BASE_URL}`);
    }
    getLeagueById(request: GetLeagueByIdRequest): Observable<LeagueDetailsDTO> {
        return this.httpClient.post(`${this.BASE_URL}/getbyemail`, request).pipe(
            map((res: LeagueDetailsDTO) => res)
        );
    }

}
