import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { CommodityControllerService, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsResponse, PageCommRegistrationsSearch, PageCompaniesResponse, PageCompaniesSearch, PageData, PageToolRegistrationsResponse, PageToolRegistrationsSearch, StorageControllerService } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { Toolreg, toToolregs } from './toolreg.model';

export class CommregControllerWrapperService extends BasePagingService<Toolreg, PageToolRegistrationsSearch> {

    constructor(
        private companyController: StorageControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageToolRegistrationsSearch>): Observable<BasePageableResponse<Toolreg>> {
        return this.companyController.pageToolRegistrations({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageToolRegistrationsResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toToolregs(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
