import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Toolreg } from './toolreg.model';

export interface ToolregState extends EntityState<Toolreg> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'toolreg' })
export class ToolregStore extends EntityStore<ToolregState> {

    constructor() {
        super();
    }

}
