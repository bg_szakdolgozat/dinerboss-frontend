import { Injectable } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { CommodityControllerService, CommodityDTO, CreateCommodityDTO, CreateCommodityResponse, DeleteCommodityRequest, DeleteCommodityResponse, ModifyCommodityResponse, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsSearch, PageToolRegistrationsSearch, StorageControllerService } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { CommregControllerWrapperService } from './toolreg-controller-wrapper.service';
import { Toolreg } from './toolreg.model';
import { ToolRegQuery } from './toolreg.query';
import { ToolregState, ToolregStore } from './toolreg.store';

@Injectable({ providedIn: 'root' })
export class ToolregService extends BasePageableAkitaService<Toolreg, ToolregState, PageToolRegistrationsSearch>{

    constructor(
        protected store: ToolregStore,
        protected query: ToolRegQuery,
        private controller: StorageControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new CommregControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }
}
