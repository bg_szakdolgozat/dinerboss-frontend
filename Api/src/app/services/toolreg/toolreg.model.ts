import { CommodityDTO, CommodityRegistrationDTO, ToolRegistrationDTO } from 'src/app/shared/openapi';
import { Commodity, toCommodity } from '../commodities/commodity.model';
import { Identifiable } from '../common/base-entity';
import { CustomerProfileDO, toCustomer } from '../customer/customer.model';
import { ToolregState } from './toolreg.store';

export interface Toolreg extends Identifiable<ToolregState> {
    date?: Date;
    quantity?: number;
    invalue?: boolean;
    commodity?: Commodity;
    customer?: CustomerProfileDO;
    storageId?: number;
}

export function toToolregs(dtos: ToolRegistrationDTO[]): Toolreg[] {
    return dtos.map(dto => toToolreg(dto));
}

export function toToolreg(dto: ToolRegistrationDTO): Toolreg {
    return {
        date: dto.date,
        quantity: dto.quantity,
        invalue: dto.invalue,
        commodity: dto.commodity,
        customer: toCustomer(dto.customer),
        storageId: dto.storageId
    } as Toolreg;
}
