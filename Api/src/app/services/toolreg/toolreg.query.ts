import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ToolregState, ToolregStore } from './toolreg.store';

@Injectable({ providedIn: 'root' })
export class ToolRegQuery extends QueryEntity<ToolregState> {

    constructor(protected store: ToolregStore) {
        super(store);
    }

}
