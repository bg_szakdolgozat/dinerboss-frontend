import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { AssignUsersToStoreRequest, AssignUsersToStoreResponse, CreateStoreDTO, CreateStoreResponse, DeleteStoreResponse, GetByStoreIdRequest, GetByStoreIdResponse, ModifyStoreResponse, PageCustomersSearch, PageStoreSearch, PageStoresResponse, StoreControllerService, StoreDTO } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { StoreControllerWrapperService } from './store-controller-wrapper.service';
import { Store, toStore } from './store.model';
import { StoreQuery } from './store.query';
import { StoreState, StoreStore } from './store.store';

@Injectable({ providedIn: 'root' })
export class StoreService extends BasePageableAkitaService<Store, StoreState, PageStoreSearch>{

    constructor(
        protected store: StoreStore,
        protected query: StoreQuery,
        private controller: StoreControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new StoreControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }

    create(dto: CreateStoreDTO): Observable<CreateStoreResponse> {
        return this.controller.createStore({ store: dto }).pipe(
            map((response: CreateStoreResponse) => {
                return response;
            })
        );
    }
    delete(id: number): Observable<number> {
        return this.controller.deleteStore({ id }).pipe(
            map((response: DeleteStoreResponse) => response.rc)
        );
    }
    getById(id: string): Store {
        return this.query.getEntity(id);
    }
    modify(dto: StoreDTO): Observable<ModifyStoreResponse> {
        return this.controller.modifyStore({ store: dto }).pipe(
            map((response: ModifyStoreResponse) => {
                return response;
            })
        );
    }
    assignUsers(dto: AssignUsersToStoreRequest): Observable<AssignUsersToStoreResponse> {
        return this.controller.assignUsers(dto).pipe(
            map((response: AssignUsersToStoreResponse) => {
                return response;
            })
        );
    }
    getCustomersByStoreId(dto: GetByStoreIdRequest): Observable<GetByStoreIdResponse> {
        return this.controller.getCustomersById(dto).pipe(
            map((response: GetByStoreIdResponse) => {
                return response;
            })
        );
    }
    getStoresByCmpIds(compIds: number[]): Observable<PageStoresResponse> {
        return this.controller.pageStores({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageStoresResponse) => {
                return response;
            })
        );
    }
    getStoresByCompIds(compIds: number[]) {
        return this.controller.pageStores({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageStoresResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toStore(dto));
                    } else {
                        this.store.add(toStore(dto));
                    }
                }
            })
        );
    }
    refreshAll(): Observable<void> {
        return this.controller.pageStores({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
            }
        }).pipe(
            map((response: PageStoresResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toStore(dto));
                    } else {
                        this.store.add(toStore(dto));
                    }
                }
            })
        );
    }
    refreshByCompIds(compIds: number[]): Observable<void> {
        return this.controller.pageStores({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageStoresResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toStore(dto));
                    } else {
                        this.store.add(toStore(dto));
                    }
                }
            })
        );
    }
}
