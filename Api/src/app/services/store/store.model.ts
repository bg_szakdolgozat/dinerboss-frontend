import { CompanyDTO, StoreDTO } from 'src/app/shared/openapi';
import { Identifiable } from '../common/base-entity';
import { Company, toCompany } from '../company/company.model';
import { StoreState } from './store.store';


export interface Store extends Identifiable<StoreState> {

    id?: number;
    name?: string;
    opened?: Date;
    location?: string;
    phoneNumber?: string;
    company?: CompanyDTO;
}
export function toStores(storeDTO: StoreDTO[]): Store[] {
    return storeDTO?.map(dto => toStore(dto));
}

export function toStore(storeDTO: StoreDTO): Store {
    return {
        id: storeDTO.id,
        name: storeDTO.name,
        opened: storeDTO.opened,
        location: storeDTO.location,
        phoneNumber: storeDTO.phoneNumber,
        company: storeDTO.company
    } as Store;
}
