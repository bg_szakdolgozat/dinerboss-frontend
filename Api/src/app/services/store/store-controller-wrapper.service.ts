import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PageStoreSearch, StoreControllerService, PageData, PageStoresResponse, PageCompaniesResponse } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { Store, toStores } from './store.model';


export class StoreControllerWrapperService extends BasePagingService<Store, PageStoreSearch> {

    constructor(
        private storeController: StoreControllerService,
        basePath: string
    ) {
        super();
        this.storeController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageStoreSearch>): Observable<BasePageableResponse<Store>> {
        return this.storeController.pageStores({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageCompaniesResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toStores(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
