import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { CommodityControllerService, CommodityDTO, CreateCommodityDTO, CreateCommodityResponse, DeleteCommodityRequest, DeleteCommodityResponse, ModifyCommodityResponse, PageCommodityResponse, PageCommoditySearch } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { CommodityControllerWrapperService } from './commodity-controller-wrapper.service';
import { Commodity, toCommodity } from './commodity.model';
import { CommodityQuery } from './commodity.query';
import { CommodityState, CommodityStore } from './commodity.store';

@Injectable({ providedIn: 'root' })
export class CommodityService extends BasePageableAkitaService<Commodity, CommodityState, PageCommoditySearch>{

    constructor(
        protected store: CommodityStore,
        protected query: CommodityQuery,
        private controller: CommodityControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new CommodityControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }

    create(dto: CreateCommodityDTO): Observable<CreateCommodityResponse> {
        return this.controller.createCommodity({ commodity: dto }).pipe(
            map((response: CreateCommodityResponse) => {
                return response;
            })
        );
    }
    modify(dto: CommodityDTO): Observable<ModifyCommodityResponse> {
        return this.controller.modifyCommodity({ commodity: dto }).pipe(
            map((response: ModifyCommodityResponse) => {
                return response;
            })
        );
    }
    delete(id: number): Observable<DeleteCommodityResponse> {
        return this.controller.deleteCommodity({ id }).pipe(
            map((response: DeleteCommodityResponse) => {
                return response;
            })
        );
    }
    getById(id: string): Commodity {
        return this.query.getEntity(id);
    }
    refreshAll(): Observable<void> {
        return this.controller.pageCommodity({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
            }
        }).pipe(
            map((response: PageCommodityResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCommodity(dto));
                    } else {
                        this.store.add(toCommodity(dto));
                    }
                }
            })
        );
    }
    refreshByCompIds(compIds: number[]): Observable<void> {
        return this.controller.pageCommodity({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageCommodityResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCommodity(dto));
                    } else {
                        this.store.add(toCommodity(dto));
                    }
                }
            })
        );
    }
    getByCpIds(compIds: number[]): Observable<PageCommodityResponse> {
        return this.controller.pageCommodity({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageCommodityResponse) => {
                return response;
            })
        );
    }

}
