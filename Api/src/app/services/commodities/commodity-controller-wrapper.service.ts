import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CommodityControllerService, PageCommodityResponse, PageCommoditySearch, PageCompaniesResponse, PageCompaniesSearch, PageData } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { Commodity, toCommodities } from './commodity.model';

export class CommodityControllerWrapperService extends BasePagingService<Commodity, PageCommoditySearch> {

    constructor(
        private companyController: CommodityControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageCommoditySearch>): Observable<BasePageableResponse<Commodity>> {
        return this.companyController.pageCommodity({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageCommodityResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toCommodities(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
