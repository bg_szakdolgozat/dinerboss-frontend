import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Commodity } from './commodity.model';

export interface CommodityState extends EntityState<Commodity> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'company' })
export class CommodityStore extends EntityStore<CommodityState> {

    constructor() {
        super();
    }

}
