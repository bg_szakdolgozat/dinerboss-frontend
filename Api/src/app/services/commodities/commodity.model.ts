import { CommodityDTO } from 'src/app/shared/openapi';
import { Identifiable } from '../common/base-entity';
import { CommodityState } from './commodity.store';

export interface Commodity extends Identifiable<CommodityState> {
    id?: number;
    name: string;
    purchasePrice?: number;
    unit?: string;
    warranty?: number;
    compId?: number;
}

export function toCommodities(organizationsDTO: CommodityDTO[]): Commodity[] {
    return organizationsDTO.map(dto => toCommodity(dto));
}

export function toCommodity(commodityDTO: CommodityDTO): Commodity {
    return {
        id: commodityDTO.id,
        name: commodityDTO.name,
        purchasePrice: commodityDTO.purchasePrice,
        unit: commodityDTO.unit,
        warranty: commodityDTO.warranty,
        compId: commodityDTO.compId
    } as Commodity;
}
