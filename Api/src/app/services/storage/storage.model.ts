import { StorageDTO, StoreDTO } from 'src/app/shared/openapi';

export interface Storage {

    id?: number;
    store?: StoreDTO;
}
export function toStorages(storeDTO: StorageDTO[]): Storage[] {
    return storeDTO.map(dto => toStorage(dto));
}

export function toStorage(storeDTO: StorageDTO): Storage {
    return {
        id: storeDTO.id,
        store: storeDTO.store
    } as Storage;
}
