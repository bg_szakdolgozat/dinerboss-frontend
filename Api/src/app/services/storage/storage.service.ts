import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { StorageControllerService, CreateStoreDTO, CreateStoreResponse, CreateStorageRequest, CreateStorageResponse, GetAllByStoreResponse, GetStorageByIdResponse, ListCommodityByStorageResponse, RegisterCommodityRequest, RegisterCommodityResponse } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class StorageService {

    constructor(

        private controller: StorageControllerService,
    ) {

        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }

    create(storeId: number): Observable<CreateStorageResponse> {
        return this.controller.createStorage({ storeId }).pipe(
            map((response: CreateStorageResponse) => {
                return response;
            })
        );
    }
    getAllByStore(storeIds: number[]): Observable<GetAllByStoreResponse> {
        return this.controller.getAllByStore({ storeIds }).pipe(
            map((response: GetAllByStoreResponse) => {
                return response;
            })
        );
    }
    getById(id: number): Observable<GetStorageByIdResponse> {
        return this.controller.getStorageById({ id }).pipe(
            map((response: GetStorageByIdResponse) => {
                return response;
            })
        );
    }
    listCommodity(storageId: number): Observable<ListCommodityByStorageResponse> {
        return this.controller.listCommodityByStorage({ storageId }).pipe(
            map((response: ListCommodityByStorageResponse) => {
                return response;
            })
        );
    }
    listTool(storageId: number): Observable<ListCommodityByStorageResponse> {
        return this.controller.listToolByStorage({ storageId }).pipe(
            map((response: ListCommodityByStorageResponse) => {
                return response;
            })
        );
    }
    registerCommodity(dto: RegisterCommodityRequest): Observable<RegisterCommodityResponse> {
        return this.controller.registerCommodity(dto).pipe(
            map((response: RegisterCommodityResponse) => {
                return response;
            })
        );
    }
    registerTool(dto: RegisterCommodityRequest): Observable<RegisterCommodityResponse> {
        return this.controller.registerTool(dto).pipe(
            map((response: RegisterCommodityResponse) => {
                return response;
            })
        );
    }
}
