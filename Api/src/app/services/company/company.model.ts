import { CompanyDTO } from 'src/app/shared/openapi';
import { Identifiable } from '../common/base-entity';
import { CustomerProfileDO } from '../customer/customer.model';
import { CompanyState } from './company.store';

export interface Company extends Identifiable<CompanyState> {

    name: string;
    founded: Date;
    address: string;
    owners: Array<CustomerProfileDO>;
}
export function toCompanies(organizationsDTO: CompanyDTO[]): Company[] {
    if (organizationsDTO) {
        return organizationsDTO.map(dto => toCompany(dto));
    } else {
        return [];
    }
}

export function toCompany(companyDTO: CompanyDTO): Company {
    return {
        id: companyDTO.id,
        name: companyDTO.name,
        founded: companyDTO.founded,
        address: companyDTO.address,
        owners: companyDTO.owners

    } as Company;
}
