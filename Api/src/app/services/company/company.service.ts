import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { AssignOwnersResponse, CompanyControllerService, CreateCompanyDTO, CreateCompanyResponse, CustomerProfileDTO, DeleteCompanyResponse, GetByCompanyIdResponse, GetByStoreIdsRequest, GetByStoreIdsResponse, GetCompanyByNameResponse, GetStoreIncomesRequest, GetStoreIncomesResponse, GetStoreReportsRequest, GetStoreReportsResponse, ModifyCompanyRequest, ModifyCompanyResponse, PageCompaniesResponse, PageCompaniesSearch } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { CustomerProfileDO } from '../customer/customer.model';
import { CompanyControllerWrapperService } from './company-controller-wrapper.service';
import { Company, toCompany } from './company.model';
import { CompanyQuery } from './company.query';
import { CompanyState, CompanyStore } from './company.store';

@Injectable({ providedIn: 'root' })
export class CompanyService extends BasePageableAkitaService<Company, CompanyState, PageCompaniesSearch>{

    constructor(
        protected store: CompanyStore,
        protected query: CompanyQuery,
        private controller: CompanyControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new CompanyControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }

    create(dto: CreateCompanyDTO): Observable<CreateCompanyResponse> {
        return this.controller.createCompany({ company: dto }).pipe(
            map((response: CreateCompanyResponse) => {
                return response;
            })
        );
    }
    delete(id: number): Observable<number> {
        return this.controller.deleteCompany({ id }).pipe(
            map((response: DeleteCompanyResponse) => response.rc)
        );
    }
    getByName(name: string): Observable<GetCompanyByNameResponse> {
        return this.controller.getCompanyByName({ name }).pipe(
            map((response: GetCompanyByNameResponse) => {
                return response;
            })
        );
    }
    getCustomersByCompId(compId: number): Observable<GetByCompanyIdResponse> {
        return this.controller.getCustomersById({ compId }).pipe(
            map((response: GetByCompanyIdResponse) => {
                return response;
            })
        );
    }
    assignOwners(customerIds: number[], companyId: number): Observable<AssignOwnersResponse> {
        return this.controller.assignOwners({ customerIds, companyId }).pipe(
            map((response: AssignOwnersResponse) => {
                return response;
            })
        );
    }
    getById(id: string): Company {
        return this.query.getEntity(id);
    }
    modify(dto: ModifyCompanyRequest): Observable<ModifyCompanyResponse> {
        return this.controller.modifyCompany(dto).pipe(
            map((response: ModifyCompanyResponse) => {
                return response;
            })
        );
    }
    getByStoreIds(dto: GetByStoreIdsRequest): Observable<GetByStoreIdsResponse> {
        return this.controller.getByStoreIds(dto).pipe(
            map((response: ModifyCompanyResponse) => {
                return response;
            })
        );
    }
    getByCompIds(compIds: number[]): Observable<PageCompaniesResponse> {
        return this.controller.pageCompanies({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageCompaniesResponse) => {
                return response;
            })
        );
    }
    refreshByCompIds(compIds: number[]): Observable<void> {
        return this.controller.pageCompanies({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                compIds
            }
        }).pipe(
            map((response: PageCompaniesResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCompany(dto));
                    } else {
                        this.store.add(toCompany(dto));
                    }
                }
            })
        );
    }
    refreshAll(): Observable<void> {
        return this.controller.pageCompanies({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
            }
        }).pipe(
            map((response: PageCompaniesResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCompany(dto));
                    } else {
                        this.store.add(toCompany(dto));
                    }
                }
            })
        );
    }
    getStoreIncomes(dto: GetStoreIncomesRequest): Observable<GetStoreIncomesResponse> {
        return this.controller.getStoreIncomes(dto).pipe(
            map((response: GetStoreIncomesResponse) => {
                return response;
            })
        );
    }
    getStoreReports(dto: GetStoreReportsRequest): Observable<GetStoreReportsResponse> {
        return this.controller.getStoreReports(dto).pipe(
            map((response: GetStoreReportsResponse) => {
                return response;
            })
        );
    }
}
