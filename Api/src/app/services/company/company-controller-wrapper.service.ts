import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompanyControllerService, PageCompaniesResponse, PageCompaniesSearch, PageData } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { Company, toCompanies } from './company.model';

export class CompanyControllerWrapperService extends BasePagingService<Company, PageCompaniesSearch> {

    constructor(
        private companyController: CompanyControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageCompaniesSearch>): Observable<BasePageableResponse<Company>> {
        return this.companyController.pageCompanies({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageCompaniesResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toCompanies(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
