import { Injectable } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { CommodityControllerService, CommodityDTO, CreateCommodityDTO, CreateCommodityResponse, DeleteCommodityRequest, DeleteCommodityResponse, ModifyCommodityResponse, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsSearch, StorageControllerService } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { CommregControllerWrapperService } from './commreg-controller-wrapper.service';
import { CommregQuery } from './commodity.query';
import { Commreg } from './commreg.model';
import { CommregState, CommregStore } from './commreg.store';

@Injectable({ providedIn: 'root' })
export class CommregService extends BasePageableAkitaService<Commreg, CommregState, PageCommRegistrationsSearch>{

    constructor(
        protected store: CommregStore,
        protected query: CommregQuery,
        private controller: StorageControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new CommregControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }
}
