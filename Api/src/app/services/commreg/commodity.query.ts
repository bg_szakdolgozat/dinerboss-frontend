import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { CommregState, CommregStore } from './commreg.store';

@Injectable({ providedIn: 'root' })
export class CommregQuery extends QueryEntity<CommregState> {

    constructor(protected store: CommregStore) {
        super(store);
    }

}
