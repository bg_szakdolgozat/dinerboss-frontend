import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { CommodityControllerService, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsResponse, PageCommRegistrationsSearch, PageCompaniesResponse, PageCompaniesSearch, PageData, StorageControllerService } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { Commreg, toCommregs } from './commreg.model';

export class CommregControllerWrapperService extends BasePagingService<Commreg, PageCommRegistrationsSearch> {

    constructor(
        private companyController: StorageControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageCommRegistrationsSearch>): Observable<BasePageableResponse<Commreg>> {
        return this.companyController.pageCommRegistrations({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageCommRegistrationsResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toCommregs(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
