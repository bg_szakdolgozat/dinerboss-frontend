import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Commreg } from './commreg.model';

export interface CommregState extends EntityState<Commreg> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'commreg' })
export class CommregStore extends EntityStore<CommregState> {

    constructor() {
        super();
    }

}
