import { CommodityDTO, CommodityRegistrationDTO } from 'src/app/shared/openapi';
import { Commodity, toCommodity } from '../commodities/commodity.model';
import { Identifiable } from '../common/base-entity';
import { CustomerProfileDO, toCustomer } from '../customer/customer.model';
import { CommregState } from './commreg.store';

export interface Commreg extends Identifiable<CommregState> {
    date?: Date;
    quantity?: number;
    invalue?: boolean;
    commodity?: Commodity;
    customer?: CustomerProfileDO;
    storageId?: number;
}

export function toCommregs(dtos: CommodityRegistrationDTO[]): Commreg[] {
    return dtos.map(dto => toCommreg(dto));
}

export function toCommreg(dto: CommodityRegistrationDTO): Commreg {
    return {
        date: dto.date,
        quantity: dto.quantity,
        invalue: dto.invalue,
        commodity: dto.commodity,
        customer: toCustomer(dto.customer),
        storageId: dto.storageId
    } as Commreg;
}
