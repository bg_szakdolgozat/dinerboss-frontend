import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { CheckoutControllerService, CommodityControllerService, PageCheckoutResponse, PageCheckoutSearch, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsResponse, PageCommRegistrationsSearch, PageCompaniesResponse, PageCompaniesSearch, PageData, StorageControllerService } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { Checkout, toCheckouts } from './checkout.model';

export class CheckoutControllerWrapperService extends BasePagingService<Checkout, PageCheckoutSearch> {

    constructor(
        private companyController: CheckoutControllerService,
        basePath: string
    ) {
        super();
        this.companyController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageCheckoutSearch>): Observable<BasePageableResponse<Checkout>> {
        return this.companyController.pageCheckout({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageCheckoutResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toCheckouts(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
