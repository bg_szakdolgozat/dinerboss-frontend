import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { CheckoutState, CheckoutStore } from './checkout.store';

@Injectable({ providedIn: 'root' })
export class CheckoutQuery extends QueryEntity<CheckoutState> {

    constructor(protected store: CheckoutStore) {
        super(store);
    }

}
