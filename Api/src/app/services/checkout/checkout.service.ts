import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import { AddTransmissionDTO, AddTransmissionResponse, CheckoutControllerService, CommodityControllerService, CommodityDTO, CreateCheckoutDTO, CreateCheckoutResponse, CreateCommodityDTO, CreateCommodityResponse, DeleteCheckoutResponse, DeleteCommodityRequest, DeleteCommodityResponse, ModifyCheckoutDTO, ModifyCheckoutResponse, ModifyCommodityResponse, PageCheckoutResponse, PageCheckoutSearch, PageCommodityResponse, PageCommoditySearch, PageCommRegistrationsSearch, StorageControllerService } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { CheckoutControllerWrapperService } from './checkout-controller-wrapper.service';
import { Checkout, toCheckout } from './checkout.model';
import { CheckoutQuery } from './checkout.query';
import { CheckoutState, CheckoutStore } from './checkout.store';

@Injectable({ providedIn: 'root' })
export class CheckoutService extends BasePageableAkitaService<Checkout, CheckoutState, PageCheckoutSearch>{

    constructor(
        protected store: CheckoutStore,
        protected query: CheckoutQuery,
        private controller: CheckoutControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new CheckoutControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }
    create(dto: CreateCheckoutDTO): Observable<CreateCheckoutResponse> {
        return this.controller.createCheckout({ checkout: dto }).pipe(
            map((response: CreateCheckoutResponse) => {
                return response;
            })
        );
    }
    modify(dto: ModifyCheckoutDTO): Observable<ModifyCheckoutResponse> {
        return this.controller.modifyCheckout({ checkout: dto }).pipe(
            map((response: ModifyCheckoutResponse) => {
                return response;
            })
        );
    }
    transmission(dto): Observable<AddTransmissionResponse> {
        return this.controller.addTransmission({ transmission: dto }).pipe(
            map((response: AddTransmissionResponse) => {
                return response;
            })
        );
    }
    delete(id: number): Observable<DeleteCheckoutResponse> {
        return this.controller.deleteCheckout({ id }).pipe(
            map((response: DeleteCheckoutResponse) => {
                return response;
            })
        );
    }
    getById(id: number): Checkout {
        return this.query.getEntity(id);
    }
    refreshAll(): Observable<void> {
        return this.controller.pageCheckout({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
            }
        }).pipe(
            map((response: PageCheckoutResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCheckout(dto));
                    } else {
                        this.store.add(toCheckout(dto));
                    }
                }
            })
        );
    }
    getChById(id: number): Observable<PageCheckoutResponse> {
        return this.controller.pageCheckout({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                storeIds: [id]
            }
        }).pipe(
            map((response: PageCheckoutResponse) => {
                return response;
            })
        );
    }

    refreshById(id): Observable<void> {
        return this.controller.pageCheckout({
            page: {
                pageNumber: 0,
                pageSize: 200,
                sortOrder: {
                }
            },
            search: {
                storeIds: [id]
            }
        }).pipe(
            map((response: PageCheckoutResponse) => {
                for (const dto of response.data) {
                    const existEntity = this.query.getEntity(dto.id);
                    if (existEntity) {
                        this.store.update(existEntity.id, toCheckout(dto));
                    } else {
                        this.store.add(toCheckout(dto));
                    }
                }
            })
        );
    }


}
