import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Checkout } from './checkout.model';

export interface CheckoutState extends EntityState<Checkout> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'checkout' })
export class CheckoutStore extends EntityStore<CheckoutState> {

    constructor() {
        super();
    }

}
