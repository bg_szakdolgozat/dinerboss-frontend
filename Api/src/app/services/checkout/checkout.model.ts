import { CheckoutDTO, StoreDTO } from 'src/app/shared/openapi';
import { Identifiable } from '../common/base-entity';
import { CheckoutState } from './checkout.store';

export interface Checkout extends Identifiable<CheckoutState> {
    id?: number;
    name: string;
    comment?: string;
    money?: number;
    store?: StoreDTO;
}

export function toCheckouts(dtos: CheckoutDTO[]): Checkout[] {
    return dtos.map(dto => toCheckout(dto));
}

export function toCheckout(dto: CheckoutDTO): Checkout {
    return {
        id: dto.id,
        name: dto.name,
        comment: dto.comment,
        money: dto.money,
        store: dto.store
    } as Checkout;
}
