import { Injectable } from '@angular/core';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Report } from './report.model';

export interface ReportState extends EntityState<Report> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'report' })
export class ReportStore extends EntityStore<ReportState> {

    constructor() {
        super();
    }

}
