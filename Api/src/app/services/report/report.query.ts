import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ReportState, ReportStore } from './report.store';

@Injectable({ providedIn: 'root' })
export class ReportQuery extends QueryEntity<ReportState> {

    constructor(protected store: ReportStore) {
        super(store);
    }

}
