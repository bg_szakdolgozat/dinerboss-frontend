import { CustomerProfileDTO, ReportDTO, StoreDTO } from 'src/app/shared/openapi';
import { Identifiable } from '../common/base-entity';
import { ReportState } from './report.store';

export interface Report extends Identifiable<ReportState> {
    id?: number;
    customer?: CustomerProfileDTO;
    store?: StoreDTO;
    income?: number;
    expense?: number;
    comment?: string;
    date?: string;
}

export function toReports(dtos: ReportDTO[]): Report[] {
    return dtos.map(dto => toReport(dto));
}

export function toReport(dto: ReportDTO): Report {
    return {
        id: dto.id,
        customer: dto.customer,
        income: dto.income,
        expense: dto.expense,
        comment: dto.comment,
        date: dto.date,
        store: dto.store
    } as Report;
}

