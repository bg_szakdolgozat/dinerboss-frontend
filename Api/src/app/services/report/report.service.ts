import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CreateReportDTO, CreateReportResponse, PageReportSearch, ReportControllerService } from 'src/app/shared/openapi';
import { environment } from 'src/environments/environment';
import { BasePageableAkitaService } from '../common/datasource-template/akita/akita.service';
import { LoaderService } from '../common/loader.service';
import { ReportControllerWrapperService } from './report-controller-wrapper.service';
import { Report } from './report.model';
import { ReportQuery } from './report.query';
import { ReportState, ReportStore } from './report.store';

@Injectable({ providedIn: 'root' })
export class ReportService extends BasePageableAkitaService<Report, ReportState, PageReportSearch>{

    constructor(
        protected store: ReportStore,
        protected query: ReportQuery,
        private controller: ReportControllerService,
        loaderService: LoaderService
    ) {
        super(query, store, new ReportControllerWrapperService(
            controller, `${environment.apiBaseUrl}`), loaderService);
        controller.configuration.basePath = `${environment.apiBaseUrl}`;
    }
    create(dto: CreateReportDTO): Observable<CreateReportResponse> {
        return this.controller.createReport({ report: dto }).pipe(
            map((response: CreateReportResponse) => {
                return response;
            })
        );
    }
}
