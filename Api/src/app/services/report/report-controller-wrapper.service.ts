import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PageData, PageReportResponse, PageReportSearch, ReportControllerService } from 'src/app/shared/openapi';
import { BasePagingService } from '../common/datasource-template/paging/base-paging.service';
import { BasePageableRequest, BasePageableResponse } from '../common/datasource-template/paging/page';
import { Report, toReports } from './report.model';

export class ReportControllerWrapperService extends BasePagingService<Report, PageReportSearch> {

    constructor(
        private reportController: ReportControllerService,
        basePath: string
    ) {
        super();
        this.reportController.configuration.basePath = basePath;
    }

    getPageableData(query: BasePageableRequest<PageReportSearch>): Observable<BasePageableResponse<Report>> {
        return this.reportController.pageReport({
            page: query.page as PageData,
            search: query.params
        }).pipe(
            map((response: PageReportResponse) => ({
                data: !response.data || response.data.length === 0 ? [] : toReports(response.data),
                totalCount: response.totalCount
            }))
        );
    }
}
