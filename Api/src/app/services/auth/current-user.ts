import { Company } from '../company/company.model';
import { Store } from '../store/store.model';

export interface CurrentUser {
    id: number;
    email?: string;
    firstName?: string;
    lastName?: string;
    role?: number;
    companies?: Company[];
    stores?: Store[];
}
const STORAGE_KEY = 'currentUser';

export class CurrentUserProfileModel {
    id: number;
    email?: string;
    firstName?: string;
    lastName?: string;
    role?: number;
    companies?: Company[];
    stores?: Store[];
    constructor() {
        let stored = sessionStorage.getItem(STORAGE_KEY);
        if (stored) {
            stored = atob(stored);
            const userProfile = JSON.parse(decodeURIComponent(escape(stored)));
            Object.assign(this, userProfile);
        }
    }
    // tslint:disable-next-line:typedef
    toJSON() {
        return JSON.stringify({
            id: this.id,
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            role: this.role,
            companies: this.companies,
            stores: this.stores
        });
    }

    public save(currentUser: CurrentUser): void {
        Object.assign(this, currentUser);
        this.saveToSession();
    }

    private saveToSession(): void {
        sessionStorage.setItem(STORAGE_KEY, btoa(unescape(encodeURIComponent(this.toJSON()))));
    }

    destroy(): void {
        sessionStorage.removeItem(STORAGE_KEY);
        this.id = undefined;
        this.email = undefined;
        this.firstName = undefined;
        this.lastName = undefined;
        this.role = undefined;
        this.companies = undefined;
        this.stores = undefined;
    }

    isBase64(str): boolean {
        if (str === '' || str.trim() === '') { return false; }
        try {
            return btoa(atob(str)) === str;
        } catch (err) {
            return false;
        }
    }
}

