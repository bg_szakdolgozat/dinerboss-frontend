export interface CurrentSuperadmin {
    id?: number;
    email?: string;
    firstName?: string;
    lastName?: string;
    role?: number;
}
const STORAGE_KEY = 'currentSuperadmin';

export class CurrentSuperadminProfileModel {
    id?: number;
    email?: string;
    firstName?: string;
    lastName?: string;
    role?: number;

    constructor() {
        let stored = sessionStorage.getItem(STORAGE_KEY);
        if (stored) {
            stored = atob(stored);
            const adminProfile = JSON.parse(decodeURIComponent(escape(stored)));
            Object.assign(this, adminProfile);
        }
    }
    // tslint:disable-next-line:typedef
    toJSON() {
        console.log('asd' + this.role);
        return JSON.stringify({
            id: this.id,
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            role: this.role
        });

    }

    public save(currentSuperadmin: CurrentSuperadmin): void {
        Object.assign(this, currentSuperadmin);
        this.saveToSession();
    }

    private saveToSession(): void {
        sessionStorage.setItem(STORAGE_KEY, btoa(unescape(encodeURIComponent(this.toJSON()))));
    }

    destroy(): void {
        sessionStorage.removeItem(STORAGE_KEY);
        this.email = undefined;
        this.firstName = undefined;
        this.lastName = undefined;
        this.role = undefined;
        this.id = undefined;
    }

    isBase64(str): boolean {
        if (str === '' || str.trim() === '') { return false; }
        try {
            return btoa(atob(str)) === str;
        } catch (err) {
            return false;
        }
    }
}
