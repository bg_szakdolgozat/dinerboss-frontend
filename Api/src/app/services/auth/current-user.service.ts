import { Injectable } from '@angular/core';
import { resetStores } from '@datorama/akita';
import { Company } from '../company/company.model';
import { Store } from '../store/store.model';
import { CurrentUser, CurrentUserProfileModel } from './current-user';

@Injectable()
export class CurrentUserService {
    currentUserProfile: CurrentUserProfileModel;
    constructor() {
        this.currentUserProfile = new CurrentUserProfileModel();
    }

    create(currentUser: CurrentUser): void {
        this.currentUserProfile = new CurrentUserProfileModel();
        this.currentUserProfile.save(currentUser);
    }

    update(currentUser: CurrentUser): void {
        this.currentUserProfile.save(currentUser);

    }

    destroyAll(): void {
        this.currentUserProfile.destroy();
        sessionStorage.clear();
        resetStores();
    }

    public getId(): number {
        return this.currentUserProfile.id;
    }
    public getEmail(): string {
        return this.currentUserProfile.email;
    }
    public getFirstName(): string {
        return this.currentUserProfile.firstName;
    }
    public getLastName(): string {
        return this.currentUserProfile.lastName;
    }
    public getRole(): number {
        return this.currentUserProfile.role;
    }

    public getCompanies(): Company[] {
        return this.currentUserProfile.companies;
    }
    public getStores(): Store[] {
        return this.currentUserProfile.stores;
    }

    isAuthorized(): boolean {
        const adminRole = 2;
        const currentRole = this.getRole();
        console.log(currentRole);

        return (adminRole <= currentRole);
    }
    isOwner() {
        return (this.getRole() === 2);
    }
}
