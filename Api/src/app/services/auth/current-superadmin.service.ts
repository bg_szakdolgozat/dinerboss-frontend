import { Injectable } from '@angular/core';
import { CurrentSuperadmin, CurrentSuperadminProfileModel } from './current-superadmin';
import { resetStores } from '@datorama/akita';

@Injectable()
export class CurrentSuperadminService {

    currentSuperadminProfile: CurrentSuperadminProfileModel;
    constructor() {
        this.currentSuperadminProfile = new CurrentSuperadminProfileModel();
    }

    create(currentUser: CurrentSuperadmin): void {
        this.currentSuperadminProfile = new CurrentSuperadminProfileModel();
        this.currentSuperadminProfile.save(currentUser);
    }

    update(currentUser: CurrentSuperadmin): void {
        this.currentSuperadminProfile.save(currentUser);

    }

    destroyAll(): void {
        this.currentSuperadminProfile.destroy();
        sessionStorage.clear();
        this.currentSuperadminProfile = new CurrentSuperadminProfileModel();
        resetStores();
    }


    public getEmail(): string {
        return this.currentSuperadminProfile.email;
    }
    public getFirstName(): string {
        return this.currentSuperadminProfile.firstName;
    }
    public getLastName(): string {
        return this.currentSuperadminProfile.lastName;
    }
    public getRole(): number {
        return this.currentSuperadminProfile.role;
    }
    isAuthorized(): boolean {

        const adminRole = 1;
        const currentRole = this.getRole();
        return (sessionStorage.getItem('currentSuperadmin') !== null);

    }
}
