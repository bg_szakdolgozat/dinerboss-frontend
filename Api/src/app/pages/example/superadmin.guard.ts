import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CurrentSuperadminService } from 'src/app/services/auth/current-superadmin.service';

@Injectable()
export class SuperadminGuard implements CanActivate {
    constructor(
        private adminProfileService: CurrentSuperadminService,
        private router: Router
    ) { }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const hasRole = this.adminProfileService.isAuthorized();
        if (hasRole) {
            return true;
        } else {
            this.router.navigate(['/']);
            return false;
        }
    }
}
