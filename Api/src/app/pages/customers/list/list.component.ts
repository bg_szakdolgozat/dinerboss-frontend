import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CustomerProfileDO } from 'src/app/services/customer/customer.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { CustomerState } from 'src/app/services/customer/customer.store';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { CustomerProfileDTO, PageCustomersSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends BaseList<CustomerProfileDO, CustomerState, PageCustomersSearch> {

  searchValueForm: FormControl = new FormControl();
  constructor(
    private service: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private storeService: StoreService,
    private currentUserService: CurrentUserService
  ) {
    super(service, router);
  }
  id: number;
  title: string;
  firstName: string;
  lastName: string;
  status: number;
  phoneNumber: string;
  email: string;
  storeIds: number[];
  initColumns(): string[] {
    return ['select', 'id', 'title', 'firstName', 'lastName', 'email', 'status', 'phoneNumber', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {
    this.sort.direction = 'asc';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    let stores: Store[] = [];
    let storeIds: number[];
    const customers: CustomerProfileDTO[] = [];
    this.storeService.getStoresByCompIds(ids).subscribe(() => {
      stores = this.storeService.getAll();
      storeIds = stores.map(s => s.id);
      this.storeIds = storeIds;
      this.loadDataSource();
    });
  }
  searchParams(): PageCustomersSearch {
    if (this.storeIds) {
      if (this.searchValueForm.value) {
        return {
          value: this.searchValueForm.value,
          storeIds: this.storeIds
        };
      }
      return {
        storeIds: this.storeIds
      };
    }

  }
  onCreate() {
    this.router.navigate(['create-customer'], { relativeTo: this.route });
  }
  onEdit(row: CustomerProfileDO) {
    this.router.navigate(['edit-customer', row.id], { relativeTo: this.route });
  }
  onDelete(row) {
    const deleteTitle = 'Felhasználó törlése';
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '500px',
      height: '200px',
      disableClose: true,
      data: deleteTitle
    });
    const dialogSub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(row.id).subscribe((resp) => {
          this.notify.successMessage('Sikeres törlés.');
          this.loadDataSource();
        });
      }
    }, () => {
      this.notify.error('Törlés sikertelen');

    });
  }
}
