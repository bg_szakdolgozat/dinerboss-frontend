import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { CreateCustomerDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  stores: Store[] = [];
  customerForm = new FormGroup({
    title: new FormControl(null),
    firstName: new FormControl(null, [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required, Validators.pattern(regexConstants.phoneNumber)]),
    store: new FormControl('', [Validators.required]),
  });
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(regexConstants.emailPattern)]),
    password: new FormControl('', [Validators.required]),
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CustomerService,
    private notify: NotificationService,
    private storeService: StoreService,
    private currentUserService: CurrentUserService
  ) { }

  ngOnInit(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.storeService.getStoresByCompIds(ids).subscribe(() => {
      this.stores = this.storeService.getAll();
    });
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  onSave() {
    const request: CreateCustomerDTO =
    {
      title: this.customerForm.get('title').value ? this.customerForm.get('title').value : undefined,
      firstName: this.customerForm.get('firstName').value,
      lastName: this.customerForm.get('lastName').value,
      phoneNumber: this.customerForm.get('phoneNumber').value,
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    };
    this.service.create(request).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Felhasználó létrehozása sikeres.');
        if (this.customerForm.get('store').value) {
          this.service.getByEmail(this.loginForm.get('email').value).subscribe((resp2) => {
            this.service.getByStoreIds([this.customerForm.get('store').value.id]).subscribe((resp3) => {
              const ids = resp3.data.map(p => p.id);
              ids.push(resp2.admin.id);
              this.storeService.assignUsers({ storeId: this.customerForm.get('store').value.id, userIds: ids }).subscribe();
              this.navigateToParent();
            });
          });
        }
      }
    }, () => {
      this.notify.error('Felhasználó létrehozása sikertelen. (Figyeljen az e-mail cím egyediségére!)');
    });
  }
}
