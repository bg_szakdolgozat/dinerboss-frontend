import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CustomersRoutingModule } from './customers-routing.module';
import { CreateComponent } from './create/create.component';
import { ModifyComponent } from './modify/modify.component';



@NgModule({
  declarations: [ListComponent, CreateComponent, ModifyComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CustomersRoutingModule,
    CommonBaseModule
  ]
})
export class CustomersModule { }
