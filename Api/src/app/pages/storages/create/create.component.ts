import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  stores: Store[] = [];
  storageForm = new FormGroup({
    store: new FormControl('', [Validators.required])
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private service: StorageService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.storeService.getStoresByCompIds(ids).subscribe(() => {
      this.stores = this.storeService.getAll();

    });

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  onCreate() {
    const storeId = this.storageForm.get('store').value.id;
    this.service.create(storeId).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Raktár sikeresen hozzáadva');
        this.navigateToParent();
      } else {
        this.notify.error('Eszköz hozzáadása sikertelen');
      }
    }, (err) => {
      this.notify.error('Eszköz hozzáadása sikertelen.');

    });
  }
}
