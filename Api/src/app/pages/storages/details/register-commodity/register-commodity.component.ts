import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { toStorage } from 'src/app/services/storage/storage.model';
import { StorageService } from 'src/app/services/storage/storage.service';
import { CommodityDTO, RegisterCommodityRequest, RegistrationDTO, StorageDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-register-commodity',
  templateUrl: './register-commodity.component.html',
  styleUrls: ['./register-commodity.component.css']
})
export class RegisterCommodityComponent implements OnInit {
  id;
  storage: StorageDTO;
  registrationForm = new FormGroup({
    commodity: new FormControl(null, [Validators.required]),
    quantity: new FormControl(null, [Validators.required]),
    in: new FormControl(false, [Validators.required])
  });
  commodities: CommodityDTO[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: StorageService,
    private notify: NotificationService,
    private commodityService: CommodityService,
    private currentUserService: CurrentUserService,
    private companyService: CompanyService
  ) { }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    if (ids.length > 0) {
      this.service.getById(this.id).subscribe((resp) => {
        this.storage = resp.store;
        this.commodityService.getByCpIds(ids).subscribe((resp3) => {
          this.commodities = resp3.data;
        });
      });
    } else {

      const storeIds = this.currentUserService.getStores().map(p => p.id);
      this.companyService.getByStoreIds({ storeIds }).subscribe((resp) => {
        const compIds = resp.companies.map(p => p.id);
        this.service.getById(this.id).subscribe((resp2) => {
          this.storage = resp2.store;
          this.commodityService.getByCpIds(compIds).subscribe((resp3) => {
            this.commodities = resp3.data;
          });
        });

      });
    }

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  registerCommodity() {
    const registration =
    {
      customerId: this.currentUserService.getId(),
      quantity: this.registrationForm.get('quantity').value,
      in: !this.registrationForm.get('in').value,
      valueId: this.registrationForm.get('commodity').value.id,
      storageId: this.storage.id
    };
    this.service.registerCommodity({ registration }).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Sikeres regisztráció.');
        this.navigateToParent();
      }
    }, () => {
      this.notify.error('Regisztráció sikertelen.');
    });
  }
}
