import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCommregComponent } from './list-commreg.component';

describe('ListCommregComponent', () => {
  let component: ListCommregComponent;
  let fixture: ComponentFixture<ListCommregComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCommregComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCommregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
