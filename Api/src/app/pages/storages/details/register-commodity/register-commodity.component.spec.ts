import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCommodityComponent } from './register-commodity.component';

describe('RegisterCommodityComponent', () => {
  let component: RegisterCommodityComponent;
  let fixture: ComponentFixture<RegisterCommodityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCommodityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCommodityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
