import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ToolService } from 'src/app/services/tool/tool.service';
import { StorageDTO, CommodityDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-register-tool',
  templateUrl: './register-tool.component.html',
  styleUrls: ['./register-tool.component.css']
})
export class RegisterToolComponent implements OnInit {
  id;
  storage: StorageDTO;
  registrationForm = new FormGroup({
    commodity: new FormControl(null, [Validators.required]),
    quantity: new FormControl(null, [Validators.required]),
    in: new FormControl(false, [Validators.required])
  });
  tools: CommodityDTO[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: StorageService,
    private notify: NotificationService,
    private toolService: ToolService,
    private currentUserService: CurrentUserService
  ) { }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.service.getById(this.id).subscribe((resp) => {
      this.storage = resp.store;
      this.toolService.refreshToolsByCompIds(ids).subscribe(() => {
        this.tools = this.toolService.getAll();
      });
    });
  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  registerCommodity() {
    const registration =
    {
      customerId: this.currentUserService.getId(),
      quantity: this.registrationForm.get('quantity').value,
      in: !this.registrationForm.get('in').value,
      valueId: this.registrationForm.get('commodity').value.id,
      storageId: this.storage.id
    };
    this.service.registerTool({ registration }).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Sikeres regisztráció.');
        this.navigateToParent();
      }
    }, () => {
      this.notify.error('Regisztráció sikertelen.');
    });
  }
}
