import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { toStorage } from 'src/app/services/storage/storage.model';
import { StorageService } from 'src/app/services/storage/storage.service';
import { StorageDTO } from 'src/app/shared/openapi';
import { ListToolregComponent } from './list-toolreg/list-toolreg.component';

@Component({
  selector: 'app-list-tools',
  templateUrl: './list-tools.component.html',
  styleUrls: ['./list-tools.component.css']
})
export class ListToolsComponent implements OnInit {
  @Input() storage: StorageDTO;
  data;
  id;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: StorageService,
    private notify: NotificationService,
    private dialog: MatDialog,
    private currentUserService: CurrentUserService

  ) { }


  ngOnInit(): void {
    if (!this.storage) {
      this.id = Number(this.route.snapshot.paramMap.get('id'));
      this.service.getById(this.id).subscribe((resp) => {
        this.storage = resp.store;
        this.service.listTool(this.storage.id).subscribe((resp2) => {
          this.data = resp2.data;
          console.log(this.data);

        });

      });
    } else {
      this.service.listTool(this.storage.id).subscribe((resp2) => {
        this.data = resp2.data;
      });
    }
  }
  isOwner() {
    return this.currentUserService.isOwner();
  }
  showHistory() {
    this.dialog.open(ListToolregComponent, {
      width: '800px',
      disableClose: true,
      data: this.storage.id
    });
  }


}
