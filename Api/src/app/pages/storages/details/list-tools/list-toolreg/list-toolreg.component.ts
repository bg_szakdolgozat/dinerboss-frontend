import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Toolreg } from 'src/app/services/toolreg/toolreg.model';
import { ToolregService } from 'src/app/services/toolreg/toolreg.service';
import { ToolregState } from 'src/app/services/toolreg/toolreg.store';
import { PageCommRegistrationsSearch, PageToolRegistrationsSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-list-toolreg',
  templateUrl: './list-toolreg.component.html',
  styleUrls: ['./list-toolreg.component.css']
})
export class ListToolregComponent extends BaseList<Toolreg, ToolregState, PageToolRegistrationsSearch> {
  searchValueForm: FormControl = new FormControl();
  id: string;
  constructor(
    private service: ToolregService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    @Inject(MAT_DIALOG_DATA) public data,

  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'date', 'name', 'invalue', 'quantity', 'who'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {
    this.sort.direction = 'desc';
    this.sort.active = 'date';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.loadDataSource();
  }

  searchParams(): PageCommRegistrationsSearch {
    if (this.searchValueForm.value) {
      return {
        storageIds: [this.data],
        value: this.searchValueForm.value
      };
    }
    return {
      storageIds: [this.data],
    };
  }
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
}

