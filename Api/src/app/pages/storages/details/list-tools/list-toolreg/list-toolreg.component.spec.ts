import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListToolregComponent } from './list-toolreg.component';

describe('ListToolregComponent', () => {
  let component: ListToolregComponent;
  let fixture: ComponentFixture<ListToolregComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListToolregComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListToolregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
