import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Storage } from 'src/app/services/storage/storage.model';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  id;
  storage: Storage;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: StorageService,
    private notify: NotificationService,
    private currentUserService: CurrentUserService
  ) { }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.service.getById(this.id).subscribe((resp) => {
      this.storage = resp.store;

    });
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  registerCommodities() {
    this.router.navigate(['register-commodity'], { relativeTo: this.route });
  }
  registerTools() {
    this.router.navigate(['register-tool'], { relativeTo: this.route });
  }
  isOwner() {
    return this.currentUserService.isOwner();
  }
}
