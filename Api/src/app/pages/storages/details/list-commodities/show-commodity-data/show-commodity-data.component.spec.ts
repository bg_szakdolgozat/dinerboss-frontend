import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowCommodityDataComponent } from './show-commodity-data.component';

describe('ShowCommodityDataComponent', () => {
  let component: ShowCommodityDataComponent;
  let fixture: ComponentFixture<ShowCommodityDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowCommodityDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowCommodityDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
