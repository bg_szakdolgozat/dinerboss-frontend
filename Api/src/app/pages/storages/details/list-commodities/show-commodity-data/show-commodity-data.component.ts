import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-show-commodity-data',
  templateUrl: './show-commodity-data.component.html',
  styleUrls: ['./show-commodity-data.component.css']
})
export class ShowCommodityDataComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {

  }

}
