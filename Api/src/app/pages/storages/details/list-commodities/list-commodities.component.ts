import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUser } from 'src/app/services/auth/current-user';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Storage } from 'src/app/services/storage/storage.model';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ListCommregComponent } from '../register-commodity/list-commreg/list-commreg.component';
import { ShowCommodityDataComponent } from './show-commodity-data/show-commodity-data.component';

@Component({
  selector: 'app-list-commodities',
  templateUrl: './list-commodities.component.html',
  styleUrls: ['./list-commodities.component.css']
})
export class ListCommoditiesComponent implements OnInit {
  @Input() storage: Storage;
  data;
  id;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: StorageService,
    private notify: NotificationService,
    private dialog: MatDialog,
    private currentUserService: CurrentUserService

  ) { }


  ngOnInit(): void {
    if (!this.storage) {
      this.id = Number(this.route.snapshot.paramMap.get('id'));
      this.service.getById(this.id).subscribe((resp) => {
        this.storage = resp.store;
        this.service.listCommodity(this.storage.id).subscribe((resp2) => {
          this.data = resp2.data;

        });

      });
    } else {
      this.service.listCommodity(this.storage.id).subscribe((resp2) => {
        this.data = resp2.data;
      });
    }
  }
  isOwner() {
    return this.currentUserService.isOwner();
  }
  showData(data) {
    const deleteTitle = 'Üzlet törlése';
    const dialogRef = this.dialog.open(ShowCommodityDataComponent, {
      width: '500px',
      height: '180px',
      disableClose: true,
      data
    });
  }
  showHistory() {
    this.dialog.open(ListCommregComponent, {
      width: '800px',
      disableClose: true,
      data: this.storage.id
    });
  }

}
