import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCommoditiesComponent } from './list-commodities.component';

describe('ListCommoditiesComponent', () => {
  let component: ListCommoditiesComponent;
  let fixture: ComponentFixture<ListCommoditiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCommoditiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCommoditiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
