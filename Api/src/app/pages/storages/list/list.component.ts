import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { IgxFilterOptions } from 'igniteui-angular';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Storage, toStorages } from 'src/app/services/storage/storage.model';
import { StorageService } from 'src/app/services/storage/storage.service';
import { StoreService } from 'src/app/services/store/store.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  storages: Storage[] = [];
  storeIds: number[];
  constructor(
    private service: StorageService,
    private storeService: StoreService,
    private currentUserService: CurrentUserService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  public ngOnInit() {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    if (ids.length > 0) {
      this.storeService.getStoresByCompIds(ids).subscribe(() => {
        const stores = this.storeService.getAll();
        const storeIds = stores.map(s => s.id);
        this.service.getAllByStore(storeIds).subscribe((resp) => {
          this.storages = resp.stores;
        });
      });
    } else {
      const storeIds = this.currentUserService.getStores().map(p => p.id);
      this.service.getAllByStore(storeIds).subscribe((resp) => {
        this.storages = resp.stores;
      });
    }

  }
  isOwner() {
    return this.currentUserService.isOwner();
  }
  openStorage(storage) {
    this.router.navigate([storage.id], { relativeTo: this.route });
  }
  onCreate() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
}
