import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { DetailsComponent } from './details/details.component';
import { RegisterCommodityComponent } from './details/register-commodity/register-commodity.component';
import { RegisterToolComponent } from './details/register-tool/register-tool.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    { path: '', component: ListComponent },
    { path: 'create', component: CreateComponent },
    { path: ':id', component: DetailsComponent },
    { path: ':id/register-commodity', component: RegisterCommodityComponent },
    { path: ':id/register-tool', component: RegisterToolComponent },

];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StoragesRoutingModule { }
