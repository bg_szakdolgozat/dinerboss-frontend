import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { StoragesRoutingModule } from './storages-routing.module';
// tslint:disable-next-line:max-line-length
import { IgxAvatarModule, IgxButtonGroupModule, IgxFilterModule, IgxIconModule, IgxInputGroupModule, IgxListModule } from 'igniteui-angular';
import { AppComponent } from 'src/app/app.component';
import { CreateComponent } from './create/create.component';
import { DetailsComponent } from './details/details.component';
import { ListCommoditiesComponent } from './details/list-commodities/list-commodities.component';
import { ListToolsComponent } from './details/list-tools/list-tools.component';
import { ShowCommodityDataComponent } from './details/list-commodities/show-commodity-data/show-commodity-data.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RegisterCommodityComponent } from './details/register-commodity/register-commodity.component';
import { RegisterToolComponent } from './details/register-tool/register-tool.component';
import { ListCommregComponent } from './details/register-commodity/list-commreg/list-commreg.component';
import { ListToolregComponent } from './details/list-tools/list-toolreg/list-toolreg.component';



@NgModule({
  bootstrap: [AppComponent],

  // tslint:disable-next-line:max-line-length
  declarations: [ListComponent, CreateComponent, DetailsComponent, ListCommoditiesComponent, ListToolsComponent, ShowCommodityDataComponent, RegisterCommodityComponent, RegisterToolComponent, ListCommregComponent, ListToolregComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    StoragesRoutingModule,
    CommonBaseModule,
    FlexLayoutModule,
    IgxAvatarModule,
    IgxFilterModule,
    IgxIconModule,
    IgxListModule,
    IgxInputGroupModule,
    IgxButtonGroupModule
  ]
})
export class StoragesModule { }
