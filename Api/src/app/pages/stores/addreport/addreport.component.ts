import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { ReportService } from 'src/app/services/report/report.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { CreateReportDTO, StoreDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-addreport',
  templateUrl: './addreport.component.html',
  styleUrls: ['./addreport.component.css']
})
export class AddreportComponent implements OnInit {

  store: Store;
  reportForm = new FormGroup({
    income: new FormControl(null, [Validators.required]),
    expense: new FormControl(null, [Validators.required]),
    comment: new FormControl(null),
    storeId: new FormControl(null, [Validators.required]),
    customerId: new FormControl(null, [Validators.required])
  });
  checkoutForm = new FormGroup({
    value: new FormControl(null),

    checkoutId: new FormControl()
  });
  checkouts;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: StoreService,
    private reportService: ReportService,
    private notify: NotificationService,
    private companyService: CompanyService,
    private currentUserService: CurrentUserService,
    private checkoutService: CheckoutService
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.checkoutService.getChById(Number(id)).subscribe((resp) => {
      this.checkouts = resp.data;
    });
    this.store = this.service.getById(id);
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    if (this.store === undefined) {
      this.service.refreshByCompIds(ids).subscribe(() => {
        this.store = this.service.getById(id);
        this.reportForm.get('storeId').setValue(this.store.id);

      });
    } else {
      this.reportForm.get('storeId').setValue(this.store.id);
    }
    this.reportForm.get('customerId').setValue(this.currentUserService.getId());
  }

  navigateToParent(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
  onCreate() {
    const dto: CreateReportDTO = {
      comment: this.reportForm.get('comment').value ? this.reportForm.get('comment').value : undefined,
      income: this.reportForm.get('income').value,
      expense: this.reportForm.get('expense').value,
      customerId: this.reportForm.get('customerId').value,
      storeId: this.reportForm.get('storeId').value
    };
    this.reportService.create(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.addMoneyToCheckout();
        this.removeMoneyFromCheckout();
        this.notify.successMessage('Jelentés sikeresen hozzáadva');
        this.navigateToParent();
      } else {
        this.notify.error('Jelentés hozzáadása sikertelen');
      }
    }, (err) => {
      this.notify.error('Jelentés hozzáadása sikertelen.');

    });
  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  addMoneyToCheckout() {
    const dto = {
      in: true,
      money: this.reportForm.get('income').value,
      comment: 'Napi forgalom bevétel.',
      checkoutId: this.checkoutForm.get('checkoutId').value.id,
      customerId: this.reportForm.get('customerId').value
    };
    this.checkoutService.transmission(dto).subscribe((resp) => {
      if (resp.rc === 0) {

      } else {
        this.notify.error('Tranzakció végrehajtása sikertelen');
      }
    }, (err) => {
      this.notify.error('Tranzakció végrehajtása sikertelen.');

    });
  }
  removeMoneyFromCheckout() {
    const dto = {
      in: false,
      money: this.reportForm.get('expense').value,
      comment: 'Napi kiadás.',
      checkoutId: this.checkoutForm.get('checkoutId').value.id,
      customerId: this.reportForm.get('customerId').value
    };
    this.checkoutService.transmission(dto).subscribe((resp) => {
      if (resp.rc === 0) {

      } else {
        this.notify.error('Tranzakció végrehajtása sikertelen');
      }
    }, (err) => {
      this.notify.error('Tranzakció végrehajtása sikertelen.');

    });
  }
  valueChange(event) {
    if (event.checked) {
      console.log('asd');

      this.checkoutForm.get('checkoutId').setValidators(Validators.required);
      this.checkoutForm.updateValueAndValidity();


    } else {
      this.checkoutForm.get('checkoutId').clearValidators();
      this.checkoutForm.get('checkoutId').setValue(null);
      this.checkoutForm.updateValueAndValidity();



    }
  }

}
