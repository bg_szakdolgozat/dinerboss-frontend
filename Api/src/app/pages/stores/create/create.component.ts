import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { StoreService } from 'src/app/services/store/store.service';
import { CreateStoreDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  companies: Company[];
  storeForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    opened: new FormControl(null, [Validators.required]),
    location: new FormControl(null, [Validators.required]),
    phoneNumber: new FormControl(null, [Validators.pattern(regexConstants.phoneNumber)]),
    company: new FormControl('', [Validators.required])
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CompanyService,
    private notify: NotificationService,
    private storeService: StoreService,
    private currentUserService: CurrentUserService
  ) { }

  ngOnInit(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.service.refreshByCompIds(ids).subscribe(() => {
      this.companies = this.service.getAll();
    });
    console.log(this.companies);

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  onCreate() {
    const dto: CreateStoreDTO = {
      name: this.storeForm.get('name').value,
      opened: this.storeForm.get('opened').value,
      location: this.storeForm.get('location').value,
      phoneNumber: this.storeForm.get('phoneNumber').value ? this.storeForm.get('phoneNumber').value : undefined,
      compId: this.storeForm.get('company').value.id
    };
    this.storeService.create(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Üzlet sikeresen létrehozva');
        this.navigateToParent();
      } else {
        this.notify.error('Üzlet létrehozása sikertelen');
      }
    }, (err) => {
      this.notify.error('Üzlet létrehozása sikertelen. Létező cégnevet adott meg.');

    });
  }

}

