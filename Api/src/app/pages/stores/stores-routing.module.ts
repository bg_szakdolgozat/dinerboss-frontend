import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddreportComponent } from './addreport/addreport.component';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { ModifyComponent } from './modify/modify.component';

const routes: Routes = [
    { path: '', component: ListComponent },
    { path: 'create', component: CreateComponent },
    { path: 'modify-store/:id', component: ModifyComponent },
    { path: 'addreport/:id', component: AddreportComponent }


];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StoresRoutingModule { }
