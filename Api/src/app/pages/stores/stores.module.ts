import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { SuperadminRoutingModule } from '../superadmin/superadmin-routing.module';
import { StoresRoutingModule } from './stores-routing.module';
import { CreateComponent } from './create/create.component';
import { ModifyComponent } from './modify/modify.component';
import { AddreportComponent } from './addreport/addreport.component';



@NgModule({
  declarations: [ListComponent, CreateComponent, ModifyComponent, AddreportComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    StoresRoutingModule,
    CommonBaseModule
  ]
})
export class StoresModule { }
