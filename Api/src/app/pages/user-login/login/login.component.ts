import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { toCompanies } from 'src/app/services/company/company.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { toStores } from 'src/app/services/store/store.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  constructor(
    private elementRef: ElementRef,
    private service: CustomerService,
    private currentUserService: CurrentUserService,
    private router: Router,
    private route: ActivatedRoute,
    private notify: NotificationService

  ) { }
  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundImage = 'url(\'assets/background.jpg\'';
  }

  doLogin() {
    this.currentUserService.destroyAll();
    const email = this.loginForm.get('email').value;
    const pw = this.loginForm.get('password').value;
    this.service.login({ email, password: pw }).subscribe((res) => {
      if (res != null && res.rc === 0) {
        this.notify.successMessage('Sikeres bejelentkezés.')
        const role = res.admin.companies && res.admin.companies.length > 0 ? 2 : res.admin.stores.length > 0 ? 3 : undefined;
        // tslint:disable-next-line:max-line-length
        this.currentUserService.create({ id: res.admin.id, email: res.admin.email, firstName: res.admin.firstName, lastName: res.admin.lastName, role, companies: toCompanies(res.admin.companies), stores: toStores(res.admin.stores) });
        if (this.currentUserService.isOwner()) {
          this.router.navigate(['/admin/'], { relativeTo: this.route });
        } else {
          this.router.navigate(['/admin/stores'], { relativeTo: this.route });
        }
      }
    }, () => {
      this.notify.error('Bejelentkezés sikertelen.');

    });
  }
}
