import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentSuperadminService } from 'src/app/services/auth/current-superadmin.service';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
  collapsedNav: boolean;
  mobileQuery: MediaQueryList;
  // tslint:disable-next-line:variable-name
  userName;
  // tslint:disable-next-line:variable-name
  private _mobileQueryListener: () => void;
  constructor(
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private service: CurrentUserService, private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.userName = this.service.getFirstName() + ' ' + this.service.getLastName();
  }
  isOwner() {
    return this.service.isOwner();
  }
  openCompanies(): void {
    this.router.navigate(['/admin']);
  }
  openStores() {
    this.router.navigate(['/admin/stores']);
  }
  doDestroy(): void {
    this.router.navigate(['/']);
    this.service.destroyAll();
  }
  openCustomers() {
    this.router.navigate(['/admin/customers']);
  }
  openCommodities() {
    this.router.navigate(['/admin/commodities']);
  }
  openStorage() {
    this.router.navigate(['/admin/storages']);
  }
  openCheckout() {
    this.router.navigate(['/admin/checkouts']);
  }
  openReport() {
    this.router.navigate(['/admin/report']);
  }
  openStatistics() {
    this.router.navigate(['/admin/statistics']);
  }
  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }


}
