import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { AssignAccountsComponent } from 'src/app/pages/superadmin/dashboard/superadmin-stores/assign-accounts/assign-accounts.component';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { StoreState } from 'src/app/services/store/store.store';
import { PageStoreSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-list-stores',
  templateUrl: './list-stores.component.html',
  styleUrls: ['./list-stores.component.css']
})
export class ListStoresComponent extends BaseList<Store, StoreState, PageStoreSearch> {
  searchValueForm: FormControl = new FormControl();
  id: string;
  storeIds: number[];
  constructor(
    private service: StoreService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private currentUserService: CurrentUserService
  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'id', 'name', 'opened', 'location', 'phoneNumber', 'company', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.service.getStoresByCompIds(ids).subscribe(() => {
      const stores = this.service.getAll();
      const storeIds = stores.map(s => s.id);
      this.storeIds = storeIds;
    });
    this.sort.direction = 'asc';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.loadDataSource();
  }
  searchParams(): PageStoreSearch {
    this.id = this.route.snapshot.paramMap.get('id');

    if (this.id) {
      if (this.searchValueForm.value) {
        return {
          compid: Number(this.id),
          value: this.searchValueForm.value
        };
      }
      return {
        compid: Number(this.id),
      };
    }
    if (this.searchValueForm.value) {
      return {
        value: this.searchValueForm.value
      };
    }
    return {
    };
  }
  // tslint:disable-next-line:typedef
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
  onCreate() {
    this.router.navigate(['create-store'], { relativeTo: this.route });
  }
  onDelete(row) {
    const deleteTitle = 'Üzlet törlése';
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '500px',
      height: '200px',
      disableClose: true,
      data: deleteTitle
    });
    const dialogSub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(row.id).subscribe((resp) => {
          this.notify.successMessage('Sikeres törlés.');
          this.loadDataSource();
        });
      }
    }, () => {
      this.notify.error('Törlés sikertelen');

    });
  }
  onEdit(row) {
    this.router.navigate(['modify-store', row.id], { relativeTo: this.route });

  }
  showCustomers(store: Store) {
    const data = {
      store,
      storeIds: this.storeIds
    };
    const dialogRef = this.dialog.open(AssignAccountsComponent, {
      width: '60%',
      disableClose: true,
      data
    });


  }
}
