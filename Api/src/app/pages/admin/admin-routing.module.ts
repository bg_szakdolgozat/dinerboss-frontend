import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompaniesComponent } from './companies/companies.component';
import { ModifyCompanyComponent } from './companies/modify-company/modify-company.component';

const routes: Routes = [
    { path: '', component: CompaniesComponent },
    { path: 'company/:id/details', component: ModifyCompanyComponent },

];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
