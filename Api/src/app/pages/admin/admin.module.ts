import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { AdminRoutingModule } from './admin-routing.module';
import { CompaniesComponent } from './companies/companies.component';
import { ModifyCompanyComponent } from './companies/modify-company/modify-company.component';
import { SuperadminModule } from '../superadmin/superadmin.module';
import { ListStoresComponent } from './companies/modify-company/list-stores/list-stores.component';



@NgModule({
  declarations: [AdminLayoutComponent, CompaniesComponent, ModifyCompanyComponent, ListStoresComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AdminRoutingModule,
    CommonBaseModule,
    SuperadminModule
  ]
})
export class AdminModule { }
