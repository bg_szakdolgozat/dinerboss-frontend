import { Route } from '@angular/compiler/src/core';
import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'src/app/menu-item';
import { CurrentSuperadminProfileModel } from 'src/app/services/auth/current-superadmin';
import { CurrentSuperadminService } from 'src/app/services/auth/current-superadmin.service';
import { SuperadminService } from 'src/app/services/domain/league/superadmin.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit, AfterViewInit {
  menuItems: MenuItem[] = [

    {
      label: 'Rólunk',
      icon: 'help'
    },
    {
      label: 'Szolgáltatások',
      icon: 'attach_money'
    },
    {
      label: 'Áraink',
      icon: 'notes'
    },
    {
      label: 'Ajánlatkérés',
      icon: 'slideshow'
    },
    {
      label: 'Bejelentkezés',
      icon: 'login'
    },
  ];
  constructor(private elementRef: ElementRef, private router: Router) { }
  ngOnInit(): void {

  }
  ngAfterViewInit(): void {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundImage = 'url(\'assets/background.jpg\'';
  }
  // tslint:disable-next-line:typedef
  loginSuperadmin() {
    this.router.navigate(['loginsuperadmin']);

  }
  loginAdmin() {
    this.router.navigate(['loginuser']);

  }

}
