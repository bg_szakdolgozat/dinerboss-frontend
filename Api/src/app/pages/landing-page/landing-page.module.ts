import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { LandingPageRoutingModule } from './landing-page-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AboutUsComponent } from './landing/about-us/about-us.component';
import { SzolgaltatasokComponent } from './landing/szolgaltatasok/szolgaltatasok.component';
import { PricesComponent } from './landing/prices/prices.component';
import { SuperadminLayoutComponent } from './landing/superadmin-layout/superadmin-layout.component';
import { MediaMatcher } from '@angular/cdk/layout';
import { SuperadminService } from 'src/app/services/domain/league/superadmin.service';
import { CurrentSuperadminService } from 'src/app/services/auth/current-superadmin.service';



@NgModule({
  declarations: [LandingComponent, AboutUsComponent, SzolgaltatasokComponent, PricesComponent, SuperadminLayoutComponent],
  imports: [

    CommonModule,
    LandingPageRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,

  ],
  providers: [MediaMatcher]
})
export class LandingPageModule { }
