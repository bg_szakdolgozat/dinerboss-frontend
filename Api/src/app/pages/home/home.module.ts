import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StandingsComponent } from './standings/standings.component';
import { MatchSummaryComponent } from './match-summary/match-summary.component';
import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';
import { MatchdetailsComponent } from './matchdetails/matchdetails.component';
import { LeagueComponent } from './league/league.component';

@NgModule({
  declarations: [
    HomeComponent,
    StandingsComponent,
    MatchSummaryComponent,
    PlayerdetailsComponent,
    MatchdetailsComponent,
    LeagueComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class HomeModule { }
