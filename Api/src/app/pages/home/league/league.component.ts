import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

export interface GoalScorerList {
  position: number;
  name: string;
  goal: number;
  assist: number;
}

const ELEMENT_DATA: GoalScorerList[] = [
  {position: 1, name: 'Son Heung-Min', goal: 10, assist: 7},
  {position: 2, name: 'Calvert-Lewin D', goal: 10, assist: 7},
  {position: 3, name: 'Bamford P.', goal: 10, assist: 7},
  {position: 4, name: 'Salah M.', goal: 10, assist: 7},
  {position: 5, name: 'Vardy J.', goal: 10, assist: 7},
  {position: 6, name: 'Carbon', goal: 10, assist: 7},
  {position: 7, name: 'Kane H.', goal: 10, assist: 7},
  {position: 8, name: 'Zaha W.', goal: 10, assist: 7},
  {position: 9, name: 'Ings D.', goal: 10, assist: 7},
  {position: 10, name: 'Mane S.', goal: 10, assist: 7},
  {position: 11, name: 'Maupay N.', goal: 10, assist: 7},
  {position: 12, name: 'Wilson C.', goal: 10, assist: 7},
  {position: 13, name: 'Jimenez R.', goal: 10, assist: 7},
  {position: 14, name: 'Grealish J.', goal: 10, assist: 7},
  {position: 15, name: 'Rodriguez J.', goal: 10, assist: 7},
  {position: 16, name: 'Fernandes B.', goal: 10, assist: 7},
  {position: 17, name: 'Jorginho', goal: 10, assist: 7},
  {position: 18, name: 'Watkins O.', goal: 10, assist: 7},
  {position: 19, name: 'Antonio M.', goal: 10, assist: 7},
  {position: 20, name: 'Bowen J.', goal: 10, assist: 7},
  {position: 21, name: 'Lacazette A.', goal: 10, assist: 7},
  {position: 22, name: 'Adams C.', goal: 10, assist: 7},
  {position: 23, name: 'Klich M.', goal: 10, assist: 7},
  {position: 24, name: 'Rashford M.', goal: 10, assist: 7},
  {position: 25, name: 'Sterling R.', goal: 10, assist: 7},
  {position: 26, name: 'Werner T.', goal: 10, assist: 7},
];

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.css']
})
export class LeagueComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'goal', 'assist'];
  dataSource = new MatTableDataSource<GoalScorerList>(ELEMENT_DATA);

  constructor() { }

  ngOnInit(): void {
  }

}
