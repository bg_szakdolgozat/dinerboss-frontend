import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

export interface TeamStandingsDto {
  position: number;
  team: string;
  playedgames: number;
  win: number;
  draw: number;
  defeat: number;
  goalratio: string;
  point: number;
}

const ELEMENT_DATA: TeamStandingsDto[] = [
  {position: 1, team: 'Everton', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 2, team: 'Liverpool', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 3, team: 'Aston Villa', playedgames: 5, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 4, team: 'Leicester', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 5, team: 'Everton', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 6, team: 'Tothanam', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 7, team: 'Leeds', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 8, team: 'Southampton', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 9, team: 'Crystal Palace', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 10, team: 'Wolwes', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 11, team: 'Chelsea', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 12, team: 'Arsenal', playedgames: 5, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 13, team: 'West Ham', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 14, team: 'Manchester City', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 15, team: 'Newcastle', playedgames: 5, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 16, team: 'Manchester United', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 17, team: 'Brighton', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 18, team: 'West Brom', playedgames: 5, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 19, team: 'Burnley', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 },
  {position: 20, team: 'Fullham', playedgames: 6, win: 4, draw: 1, defeat: 1, goalratio: '14-9', point: 13 }
];

@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.css']
})
export class StandingsComponent implements OnInit {

  displayedColumns: string[] = ['position', 'team', 'playedgames', 'win', 'draw', 'defeat', 'goalratio', 'point' ];
  dataSource = ELEMENT_DATA;

  constructor() { }

  // tslint:disable-next-line: member-ordering
  @ViewChild('sidenav') sidenav: MatSidenav;
  // tslint:disable-next-line: member-ordering
  isExpanded = true;
  // tslint:disable-next-line: member-ordering
  showSubmenu = false;
  // tslint:disable-next-line: member-ordering
  isShowing = false;
  showSubSubMenu = false;

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  mouseenter() {
    if (!this.isExpanded) {
      this.isShowing = true;
    }
  }

  // tslint:disable-next-line: typedef
  mouseleave() {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }

}
