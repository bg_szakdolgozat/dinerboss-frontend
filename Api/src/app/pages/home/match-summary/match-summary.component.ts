import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

export interface Statistics {
  hometeam: number;
  type: string;
  awayteam: number;
}

const ELEMENT_DATA: Statistics[] = [
  {hometeam: 38, type: 'Labdabirtoklás', awayteam: 62},
  {hometeam: 4, type: 'Kapura lövés', awayteam: 7},
  {hometeam: 2, type: 'Kaput eltaláló lövés', awayteam: 5},
  {hometeam: 2, type: 'Blokkolt lövés', awayteam: 2},
  {hometeam: 5, type: 'Szabadrugás', awayteam: 3},
  {hometeam: 1, type: 'Szöglet', awayteam: 9},
  {hometeam: 1, type: 'Les', awayteam: 0},
  {hometeam: 5, type: 'Védés', awayteam: 3},
  {hometeam: 6, type: 'Szabálytalanság', awayteam: 3},
  {hometeam: 3, type: 'Sárga lap', awayteam: 1},
  {hometeam: 382, type: 'Összes passz', awayteam: 411},
];

@Component({
  selector: 'app-match-summary',
  templateUrl: './match-summary.component.html',
  styleUrls: ['./match-summary.component.css']
})
export class MatchSummaryComponent implements OnInit {

  constructor() { }

  displayedColumns: string[] = ['hometeam', 'type', 'awayteam'];
  dataSource = ELEMENT_DATA;

    // tslint:disable-next-line: member-ordering
    @ViewChild('sidenav') sidenav: MatSidenav;
    // tslint:disable-next-line: member-ordering
    isExpanded = true;
    // tslint:disable-next-line: member-ordering
    showSubmenu = false;
    // tslint:disable-next-line: member-ordering
    isShowing = false;
    showSubSubMenu = false;

  ngOnInit(): void {
  }
    // tslint:disable-next-line: typedef
    mouseenter() {
      if (!this.isExpanded) {
        this.isShowing = true;
      }
    }
    // tslint:disable-next-line: typedef
    mouseleave() {
      if (!this.isExpanded) {
        this.isShowing = false;
      }
    }

}
