import { Component, OnInit } from '@angular/core';
import { LeagueService } from 'src/app/services/domain/league/superadmin.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Demo';
  greeting = {};

  constructor(private app: LeagueService, private http: HttpClient) {
  }
  ngOnInit(): void {

  }

  // tslint:disable-next-line:typedef
  authenticated() { return this.app.authenticated; }


}
