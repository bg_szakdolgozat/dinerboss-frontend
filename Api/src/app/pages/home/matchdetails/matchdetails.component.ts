import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';

export interface Statistics {
  hometeam: number;
  type: string;
  awayteam: number;
}

const ELEMENT_DATA: Statistics[] = [
  {hometeam: 38, type: 'Labdabirtoklás', awayteam: 62},
  {hometeam: 4, type: 'Kapura lövés', awayteam: 7},
  {hometeam: 2, type: 'Kaput eltaláló lövés', awayteam: 5},
  {hometeam: 2, type: 'Blokkolt lövés', awayteam: 2},
  {hometeam: 5, type: 'Szabadrugás', awayteam: 3},
  {hometeam: 1, type: 'Szöglet', awayteam: 9},
  {hometeam: 1, type: 'Les', awayteam: 0},
  {hometeam: 5, type: 'Védés', awayteam: 3},
  {hometeam: 6, type: 'Szabálytalanság', awayteam: 3},
  {hometeam: 3, type: 'Sárga lap', awayteam: 1},
  {hometeam: 382, type: 'Összes passz', awayteam: 411},
];

@Component({
  selector: 'app-matchdetails',
  templateUrl: './matchdetails.component.html',
  styleUrls: ['./matchdetails.component.css']
})
export class MatchdetailsComponent implements OnInit {

  displayedColumns: string[] = ['hometeam', 'type', 'awayteam'];
  dataSource = ELEMENT_DATA;

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor() { }

  ngOnInit(): void {
  }

}
