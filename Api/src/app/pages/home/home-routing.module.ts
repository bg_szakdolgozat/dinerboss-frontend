import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LeagueComponent } from './league/league.component';
import { MatchSummaryComponent } from './match-summary/match-summary.component';
import { MatchdetailsComponent } from './matchdetails/matchdetails.component';
import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';
import { StandingsComponent } from './standings/standings.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'standings', component: StandingsComponent },
  { path: 'summary', component: MatchSummaryComponent },
  { path: 'playerdetails', component: PlayerdetailsComponent },
  { path: 'matchdetails', component: MatchdetailsComponent },
  { path: 'league', component: LeagueComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
