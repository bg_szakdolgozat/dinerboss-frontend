import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CustomersRoutingModule } from '../customers/customers-routing.module';
import { ReportRoutingModule } from './report-routing.module';



@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ReportRoutingModule,
    CommonBaseModule
  ]
})
export class ReportModule { }
