import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Report } from 'src/app/services/report/report.model';
import { ReportService } from 'src/app/services/report/report.service';
import { ReportState } from 'src/app/services/report/report.store';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { CustomerProfileDTO, PageCustomersSearch, PageReportSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends BaseList<Report, ReportState, PageReportSearch> {

  searchValueForm: FormControl = new FormControl();
  storeForm: FormControl = new FormControl(null);

  constructor(
    private service: ReportService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private storeService: StoreService,
    private currentUserService: CurrentUserService
  ) {
    super(service, router);
  }
  stores;
  storeIds: number[];
  initColumns(): string[] {
    return ['select', 'id', 'date', 'income', 'expense', 'comment', 'customer', 'store'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
    this.searchControlInit(this.storeForm);

  }
  initDataSource(): void {
    this.sort.direction = 'desc';
    this.sort.active = 'date';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    let stores: Store[] = [];
    let storeIds: number[];
    const customers: CustomerProfileDTO[] = [];
    this.storeService.getStoresByCompIds(ids).subscribe(() => {
      stores = this.storeService.getAll();
      this.stores = this.storeService.getAll();
      storeIds = stores.map(s => s.id);
      this.storeIds = storeIds;
      this.loadDataSource();
    });
  }
  searchParams(): PageReportSearch {
    if (this.storeIds) {
      if (this.searchValueForm.value) {
        return {
          value: this.searchValueForm.value,
          storeIds: this.storeForm.value ? [this.storeForm.value.id] : this.storeIds,
        };
      }
      return {
        storeIds: this.storeForm.value ? [this.storeForm.value.id] : this.storeIds,
      };
    }

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
}
