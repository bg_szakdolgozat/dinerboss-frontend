import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CheckoutRoutingModule } from './checkout-routing.module';
import { CreateCheckoutComponent } from './list/create-checkout/create-checkout.component';
import { ModifyCheckoutComponent } from './list/modify-checkout/modify-checkout.component';
import { TransactionComponent } from './transaction/transaction.component';
import { HistoryComponent } from './history/history.component';



@NgModule({
  declarations: [ListComponent, CreateCheckoutComponent, ModifyCheckoutComponent, TransactionComponent, HistoryComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CheckoutRoutingModule,
    CommonBaseModule
  ]
})
export class CheckoutModule { }
