
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateCheckoutComponent } from './list/create-checkout/create-checkout.component';
import { ListComponent } from './list/list.component';
import { ModifyCheckoutComponent } from './list/modify-checkout/modify-checkout.component';
import { TransactionComponent } from './transaction/transaction.component';

const routes: Routes = [
    { path: '', component: ListComponent },
    { path: 'create', component: CreateCheckoutComponent },
    { path: ':id/edit', component: ModifyCheckoutComponent },
    { path: ':id/transaction', component: TransactionComponent },


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CheckoutRoutingModule {
}
