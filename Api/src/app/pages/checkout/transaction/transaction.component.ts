
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { AddTransmissionDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  id: number;
  transactionForm = new FormGroup({
    money: new FormControl(null, [Validators.required]),
    comment: new FormControl(null, [Validators.pattern(regexConstants.name)]),
    invalue: new FormControl(false, [Validators.required]),
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CheckoutService,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private companyService: CompanyService
  ) { }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));

  }
  onEdit() {
    const dto = {
      in: !this.transactionForm.get('invalue').value,
      money: this.transactionForm.get('money').value,
      comment: this.transactionForm.get('comment').value,
      checkoutId: this.id,
      customerId: this.currentUserService.getId()
    };
    this.service.transmission(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Tranzakció sikeresen végrehajtva.');
        this.navigateToParent();
      } else {
        this.notify.error('Tranzakció végrehajtása sikertelen');
      }
    }, (err) => {
      this.notify.error('Tranzakció végrehajtása sikertelen.');

    });
  }
  navigateToParent(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
}
