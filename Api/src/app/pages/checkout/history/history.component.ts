import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { CheckoutHistory } from 'src/app/services/checkouthistory/checkouthistory.model';
import { CheckoutHistoryService } from 'src/app/services/checkouthistory/checkouthistory.service';
import { CheckoutHistoryState } from 'src/app/services/checkouthistory/checkouthistory.store';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Commreg } from 'src/app/services/commreg/commreg.model';
import { CommregService } from 'src/app/services/commreg/commreg.service';
import { CommregState } from 'src/app/services/commreg/commreg.store';
import { PageCommRegistrationsSearch, PageTransmissionSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent extends BaseList<CheckoutHistory, CheckoutHistoryState, PageTransmissionSearch> {
  searchValueForm: FormControl = new FormControl();
  id: string;
  constructor(
    private service: CheckoutHistoryService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    @Inject(MAT_DIALOG_DATA) public data,

  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'date', 'money', 'comment', 'invalue', 'who'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {
    this.sort.direction = 'desc';
    this.sort.active = 'date';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.loadDataSource();
    console.log(this.dataSource);

  }

  searchParams(): PageTransmissionSearch {
    if (this.searchValueForm.value) {
      return {
        checkoutIds: [this.data],
        value: this.searchValueForm.value
      };
    }
    return {
      checkoutIds: [this.data],
    };
  }
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
}
