import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { CheckoutState } from 'src/app/services/checkout/checkout.store';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { ToolService } from 'src/app/services/tool/tool.service';
import { CreateCheckoutDTO, CreateCommodityDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-create-checkout',
  templateUrl: './create-checkout.component.html',
  styleUrls: ['./create-checkout.component.css']
})
export class CreateCheckoutComponent implements OnInit {

  stores: Store[];
  checkoutForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    comment: new FormControl(null, [Validators.pattern(regexConstants.name)]),
    store: new FormControl(null, [Validators.required]),
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CheckoutService,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.storeService.getStoresByCompIds(ids).subscribe(() => {
      this.stores = this.storeService.getAll();
    });

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }



  onCreate() {
    const dto: CreateCheckoutDTO = {
      name: this.checkoutForm.get('name').value,
      comment: this.checkoutForm.get('comment').value,
      storeId: this.checkoutForm.get('store').value.id

    };
    this.service.create(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Kassza sikeresen létrehozva');
        this.navigateToParent();
      } else {
        this.notify.error('Kassza létrehozása sikertelen');
      }
    }, (err) => {
      this.notify.error('Kassza létrehozása sikertelen.');

    });
  }

}
