import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { Checkout } from 'src/app/services/checkout/checkout.model';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { CheckoutState } from 'src/app/services/checkout/checkout.store';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { StoreService } from 'src/app/services/store/store.service';
import { ToolService } from 'src/app/services/tool/tool.service';
import { PageCheckoutSearch, PageStoreSearch } from 'src/app/shared/openapi';
import { HistoryComponent } from '../history/history.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends BaseList<Checkout, CheckoutState, PageCheckoutSearch> {
  searchValueForm: FormControl = new FormControl();
  id: string;
  blur = false;
  storeIds: number[];
  constructor(
    private service: CheckoutService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private storeService: StoreService
  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'id', 'name', 'comment', 'money', 'store', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  changeBlur() {
    this.blur = !this.blur;
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {

    const ids = this.currentUserService.getCompanies().map(p => p.id);
    if (ids.length > 0) {
      this.storeService.getStoresByCompIds(ids).subscribe(() => {
        const stores = this.storeService.getAll();
        this.storeIds = stores.map(s => s.id);
        this.loadDataSource();
        this.sort.direction = 'asc';
        this.paginator.pageIndex = 0;
        this.paginator.pageSize = 10;
      });
    } else {
      this.loadDataSource();
      this.paginator.pageIndex = 0;
      this.paginator.pageSize = 10;
    }

  }
  isOwner() {
    return this.currentUserService.isOwner();
  }
  searchParams(): PageCheckoutSearch {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    const storeIds = this.currentUserService.getStores().map(p => p.id);
    if (ids.length > 0) {
      if (this.searchValueForm.value) {
        return {
          storeIds: this.storeIds,
          value: this.searchValueForm.value
        };
      }
      return {
        storeIds: this.storeIds,
      };
    } else {
      if (this.searchValueForm.value) {
        return {
          storeIds,
          value: this.searchValueForm.value
        };
      }
      return {
        storeIds
      };
    }

  }

  // tslint:disable-next-line:typedef
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }

  onCreate() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
  onEdit(row) {
    this.router.navigate([row.id, 'edit'], { relativeTo: this.route });
  }
  onTransaction(row) {
    this.router.navigate([row.id, 'transaction'], { relativeTo: this.route });
  }
  onHistory(row) {
    const dialogRef = this.dialog.open(HistoryComponent, {
      width: '800px',
      disableClose: true,
      data: row.id
    });
  }
  onDelete(row) {
    const deleteTitle = 'Kassza törlése';
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '500px',
      height: '200px',
      disableClose: true,
      data: deleteTitle
    });
    const dialogSub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(row.id).subscribe((resp) => {
          this.notify.successMessage('Sikeres törlés.');
          this.loadDataSource();
        });
      }
    }, () => {
      this.notify.error('Törlés sikertelen');

    });
  }

}
