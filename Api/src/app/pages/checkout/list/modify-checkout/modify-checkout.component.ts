import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { Checkout } from 'src/app/services/checkout/checkout.model';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { CreateCheckoutDTO, ModifyCheckoutDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-modify-checkout',
  templateUrl: './modify-checkout.component.html',
  styleUrls: ['./modify-checkout.component.css']
})
export class ModifyCheckoutComponent implements OnInit {
  checkout: Checkout;
  stores: Store[];
  id;
  checkoutForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    comment: new FormControl(null, [Validators.pattern(regexConstants.name)]),
    store: new FormControl(null, [Validators.required]),
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CheckoutService,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.storeService.getStoresByCompIds(ids).subscribe(() => {
      this.stores = this.storeService.getAll();
      this.id = Number(this.route.snapshot.paramMap.get('id'));
      this.checkout = this.service.getById(this.id.toString());
      if (this.checkout === undefined) {
        this.service.refreshAll().subscribe(() => {
          this.checkout = this.service.getById(this.id.toString());
          this.fillForm();
          this.storeService.refreshAll().subscribe(() => {
            this.checkoutForm.get('store').setValue(this.storeService.getById(this.checkout.store.id.toString()));
          });
        });
      } else {
        this.fillForm();

      }
    });
  }
  fillForm() {
    this.checkoutForm.get('name').setValue(this.checkout.name);
    this.checkoutForm.get('comment').setValue(this.checkout.comment);
    this.checkoutForm.get('store').setValue(this.storeService.getById(this.checkout.store.id.toString()));
  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }



  onEdit() {
    const dto: ModifyCheckoutDTO = {
      id: this.checkout.id,
      money: this.checkout.money,
      name: this.checkoutForm.get('name').value,
      comment: this.checkoutForm.get('comment').value,
      storeId: this.checkoutForm.get('store').value.id

    };
    this.service.modify(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Kassza sikeresen módosítva');
        this.navigateToParent();
      } else {
        this.notify.error('Kassza módosítása sikertelen');
      }
    }, (err) => {
      this.notify.error('Kassza módosítása sikertelen.');

    });
  }

}
