import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyCheckoutComponent } from './modify-checkout.component';

describe('ModifyCheckoutComponent', () => {
  let component: ModifyCheckoutComponent;
  let fixture: ComponentFixture<ModifyCheckoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyCheckoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
