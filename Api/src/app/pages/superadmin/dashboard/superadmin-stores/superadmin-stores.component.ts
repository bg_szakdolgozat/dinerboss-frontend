import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { CompanyState } from 'src/app/services/company/company.store';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { StoreState } from 'src/app/services/store/store.store';
import { AssignUsersToStoreRequest, PageCompaniesSearch, PageStoreSearch } from 'src/app/shared/openapi';
import { AssignAccountsComponent } from './assign-accounts/assign-accounts.component';

@Component({
  selector: 'app-superadmin-stores',
  templateUrl: './superadmin-stores.component.html',
  styleUrls: ['./superadmin-stores.component.css']
})
export class SuperadminStoresComponent extends BaseList<Store, StoreState, PageStoreSearch> {
  searchValueForm: FormControl = new FormControl();
  id: string;
  constructor(
    private service: StoreService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService

  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'id', 'name', 'opened', 'location', 'phoneNumber', 'company', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {

    this.sort.direction = 'asc';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.loadDataSource();
  }
  searchParams(): PageStoreSearch {
    this.id = this.route.snapshot.paramMap.get('id');

    if (this.id) {
      if (this.searchValueForm.value) {
        return {
          compid: Number(this.id),
          value: this.searchValueForm.value
        };
      }
      return {
        compid: Number(this.id),
      };
    }
    if (this.searchValueForm.value) {
      return {
        value: this.searchValueForm.value
      };
    }
    return {
    };
  }
  // tslint:disable-next-line:typedef
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
  onCreate() {
    this.router.navigate(['create-store'], { relativeTo: this.route });
  }
  onDelete(row) {
    const deleteTitle = 'Üzlet törlése';
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '500px',
      height: '200px',
      disableClose: true,
      data: deleteTitle
    });
    const dialogSub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(row.id).subscribe((resp) => {
          this.notify.successMessage('Sikeres törlés.');
          this.loadDataSource();
        });
      }
    }, () => {
      this.notify.error('Törlés sikertelen');

    });
  }
  onEdit(row) {
    this.router.navigate(['modify-store', row.id], { relativeTo: this.route });

  }
  showCustomers(store: Store) {
    const data = { store };
    const dialogRef = this.dialog.open(AssignAccountsComponent, {
      width: '60%',
      disableClose: true,
      data
    });


  }
}
