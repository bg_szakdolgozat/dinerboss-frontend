import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { ModifyStoreRequest, StoreDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-modify-store',
  templateUrl: './modify-store.component.html',
  styleUrls: ['./modify-store.component.css']
})
export class ModifyStoreComponent implements OnInit {
  store: Store;
  companies: Company[];
  storeForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    opened: new FormControl(null, [Validators.required]),
    location: new FormControl(null, [Validators.required]),
    phoneNumber: new FormControl(null, [Validators.pattern(regexConstants.phoneNumber)]),
    company: new FormControl('', [Validators.required])
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: StoreService,
    private notify: NotificationService,
    private companyService: CompanyService,
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.store = this.service.getById(id);
    this.companies = this.companyService.getAll();
    this.storeForm.get('name').setValue(this.store.name);
    this.storeForm.get('opened').setValue(this.store.opened);
    this.storeForm.get('location').setValue(this.store.location);
    this.storeForm.get('phoneNumber').setValue(this.store.phoneNumber);
    this.storeForm.get('company').setValue(this.store.company);

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
  onCreate() {
    const dto: StoreDTO = {
      id: this.store.id,
      name: this.storeForm.get('name').value,
      opened: this.storeForm.get('opened').value,
      location: this.storeForm.get('location').value,
      phoneNumber: this.storeForm.get('phoneNumber').value ? this.storeForm.get('phoneNumber').value : undefined,
      company: this.storeForm.get('company').value
    };
    this.service.modify(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Üzlet sikeresen módosítva');
        this.navigateToParent();
      } else {
        this.notify.error('Üzlet módosítása sikertelen');
      }
    }, (err) => {
      this.notify.error('Üzlet módosítása sikertelen.');

    });
  }
}
