import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyStoreComponent } from './modify-store.component';

describe('ModifyStoreComponent', () => {
  let component: ModifyStoreComponent;
  let fixture: ComponentFixture<ModifyStoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyStoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
