import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperadminStoresComponent } from './superadmin-stores.component';

describe('SuperadminStoresComponent', () => {
  let component: SuperadminStoresComponent;
  let fixture: ComponentFixture<SuperadminStoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperadminStoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperadminStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
