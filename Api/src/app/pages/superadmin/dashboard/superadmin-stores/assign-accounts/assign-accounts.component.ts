import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CustomerProfileDO, toCustomers } from 'src/app/services/customer/customer.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';

@Component({
  selector: 'app-assign-accounts',
  templateUrl: './assign-accounts.component.html',
  styleUrls: ['./assign-accounts.component.css']
})
export class AssignAccountsComponent implements OnInit {
  availableCustomerList: CustomerProfileDO[] = [];
  selectedCustomerList: CustomerProfileDO[] = [];

  ownerForm = new FormGroup({
    owners: new FormControl(null, [Validators.required])
  });
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private service: StoreService,
    private customerService: CustomerService,
    private notify: NotificationService
  ) { }

  ngOnInit(): void {
    this.availableCustomerList = this.customerService.getAll();
    this.customerService.refreshAll().subscribe(() => {
      this.availableCustomerList = this.customerService.getAll();

    });
    if (this.data.storeIds) {
      console.log(this.data.storeIds);

      this.customerService.getByStoreIds(this.data.storeIds).subscribe((resp) => {
        this.availableCustomerList = toCustomers(resp.data);
      }, () => {
        this.availableCustomerList = [];
      });
    }
    if (this.availableCustomerList.length > 0) {
      this.service.getCustomersByStoreId({ storeId: this.data.store.id }).subscribe((resp) => {
        this.selectedCustomerList = toCustomers(resp.customers);
      }, () => {

      });
    }

  }

  customerChanged($event) {
    if ($event.length === 0) {
      this.ownerForm.get('owners').setValue(null);
    } else {
      this.ownerForm.get('owners').setValue(true);
    }
    this.selectedCustomerList = $event;
  }
  saveUsers() {
    const userIds = this.selectedCustomerList.map(p => p.id);

    this.service.assignUsers({ userIds, storeId: this.data.store.id }).subscribe((resp) => {
      this.notify.successMessage('Felhasználók hozzárendelése sikeres.');
    }, () => {
      this.notify.error('Sikertelen hozzárendelés.')
    });
  }
}
