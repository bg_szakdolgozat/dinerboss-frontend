import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignAccountsComponent } from './assign-accounts.component';

describe('AssignAccountsComponent', () => {
  let component: AssignAccountsComponent;
  let fixture: ComponentFixture<AssignAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignAccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
