import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@datorama/akita';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CustomerProfileDO } from 'src/app/services/customer/customer.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { CustomerState } from 'src/app/services/customer/customer.store';
import { StoreService } from 'src/app/services/store/store.service';
import { StoreState } from 'src/app/services/store/store.store';
import { PageCustomersSearch, PageStoreSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-superadmin-customers',
  templateUrl: './superadmin-customers.component.html',
  styleUrls: ['./superadmin-customers.component.css']
})
export class SuperadminCustomersComponent extends BaseList<CustomerProfileDO, CustomerState, PageCustomersSearch> {

  searchValueForm: FormControl = new FormControl();
  constructor(
    private service: CustomerService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService

  ) {
    super(service, router);
  }
  id: number;
  title: string;
  firstName: string;
  lastName: string;
  status: number;
  phoneNumber: string;
  email: string;
  initColumns(): string[] {
    return ['select', 'id', 'title', 'firstName', 'lastName', 'email', 'status', 'phoneNumber', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {
    this.sort.direction = 'asc';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.loadDataSource();
  }
  searchParams(): PageCustomersSearch {
    if (this.searchValueForm.value) {
      return {
        value: this.searchValueForm.value
      };
    }
    return {
    };
  }
  onCreate() {
    this.router.navigate(['create-customer'], { relativeTo: this.route });
  }
  onEdit(row: CustomerProfileDO) {
    this.router.navigate(['edit-customer', row.id], { relativeTo: this.route });
  }
}
