import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperadminCustomersComponent } from './superadmin-customers.component';

describe('SuperadminCustomersComponent', () => {
  let component: SuperadminCustomersComponent;
  let fixture: ComponentFixture<SuperadminCustomersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperadminCustomersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperadminCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
