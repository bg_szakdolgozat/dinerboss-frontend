import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CustomerProfileDO } from 'src/app/services/customer/customer.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { CustomerProfileDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {
  customerForm = new FormGroup({
    title: new FormControl(null),
    firstName: new FormControl(null, [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required, Validators.pattern(regexConstants.phoneNumber)]),
    email: new FormControl('', [Validators.required, Validators.pattern(regexConstants.emailPattern)]),
    password: new FormControl(''),
  });
  customer: CustomerProfileDO;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CustomerService,
    private notify: NotificationService
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.customer = this.service.getById(id);
    console.log(this.customer);
    this.customer.title ? this.customerForm.get('title').setValue(this.customer.title) : this.customerForm.get('title').setValue(undefined);
    this.customerForm.get('title').setValue(this.customer.title);
    this.customerForm.get('firstName').setValue(this.customer.firstName);
    this.customerForm.get('lastName').setValue(this.customer.lastName);
    this.customerForm.get('phoneNumber').setValue(this.customer.phoneNumber);
    this.customerForm.get('email').setValue(this.customer.email);

  }
  navigateToParent(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
  onEdit() {
    const request: CustomerProfileDTO =
    {
      id: this.customer.id,
      status: 1,
      title: this.customerForm.get('title').value ? this.customerForm.get('title').value : undefined,
      firstName: this.customerForm.get('firstName').value,
      lastName: this.customerForm.get('lastName').value,
      phoneNumber: this.customerForm.get('phoneNumber').value,
      email: this.customerForm.get('email').value,
    };
    this.service.modify(request).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Felhasználó módosítása sikeres.');
        this.navigateToParent();
      }
    }, () => {
      this.notify.error('Felhasználó módosítása sikertelen. (Figyeljen az e-mail cím egyediségére!)');
    });
  }
}

