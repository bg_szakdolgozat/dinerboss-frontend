import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { CreateCustomerDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {

  customerForm = new FormGroup({
    title: new FormControl(null),
    firstName: new FormControl(null, [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required, Validators.pattern(regexConstants.phoneNumber)]),
  });
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(regexConstants.emailPattern)]),
    password: new FormControl('', [Validators.required]),
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CustomerService,
    private notify: NotificationService
  ) { }

  ngOnInit(): void {
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  onSave() {
    const request: CreateCustomerDTO =
    {
      title: this.customerForm.get('title').value ? this.customerForm.get('title').value : undefined,
      firstName: this.customerForm.get('firstName').value,
      lastName: this.customerForm.get('lastName').value,
      phoneNumber: this.customerForm.get('phoneNumber').value,
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    };
    this.service.create(request).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Felhasználó létrehozása sikeres.');
        this.navigateToParent();
      }
    }, () => {
      this.notify.error('Felhasználó létrehozása sikertelen. (Figyeljen az e-mail cím egyediségére!)');
    });
  }
}
