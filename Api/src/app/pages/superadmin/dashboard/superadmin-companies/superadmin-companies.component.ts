import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { CompanyState } from 'src/app/services/company/company.store';
import { PageCompaniesSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-superadmin-companies',
  templateUrl: './superadmin-companies.component.html',
  styleUrls: ['./superadmin-companies.component.scss']
})
export class SuperadminCompaniesComponent extends BaseList<Company, CompanyState, PageCompaniesSearch> {
  searchValueForm: FormControl = new FormControl();

  constructor(
    private service: CompanyService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService

  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'id', 'name', 'address', 'founded', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {
    this.sort.active = 'name';
    this.sort.direction = 'asc';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.loadDataSource();
  }
  searchParams(): PageCompaniesSearch {
    if (this.searchValueForm.value) {
      return {
        value: this.searchValueForm.value
      };
    }
    return {
    };
  }
  // tslint:disable-next-line:typedef
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
  onCreate() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
  onDelete(row) {
    const deleteTitle = 'Cég törlése';
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '500px',
      height: '200px',
      disableClose: true,
      data: deleteTitle
    });
    const dialogSub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(row.id).subscribe((resp) => {
          this.notify.successMessage('Sikeres törlés.');
          this.loadDataSource();
        });
      }
    }, () => {
      this.notify.error('Törlés sikertelen');

    });
  }
  onDetails(row: Company) {
    this.router.navigate(['company', row.id, 'details'], { relativeTo: this.route });

  }
}
