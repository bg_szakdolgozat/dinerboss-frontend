import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { CustomerProfileDO, toCustomers } from 'src/app/services/customer/customer.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { ModifyCompanyRequest } from 'src/app/shared/openapi';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css']
})
export class CompanyDetailsComponent implements OnInit {
  company: Company;
  availableCustomerList: CustomerProfileDO[] = [];
  selectedCustomerList: CustomerProfileDO[] = [];
  ownerForm = new FormGroup({
    owners: new FormControl(null, [Validators.required])
  });
  companyForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    founded: new FormControl(null, [Validators.required]),
    address: new FormControl('', [Validators.required])
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CompanyService,
    private customerService: CustomerService,
    private notify: NotificationService,
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);

    this.company = this.service.getById(id);
    if (!this.company) {
      this.service.refreshByCompIds([Number(id)]).subscribe(() => {
        this.company = this.service.getById(id);
        this.companyForm.get('name').setValue(this.company.name);
        this.companyForm.get('name').disable();
        this.companyForm.get('founded').setValue(this.company.founded);
        this.companyForm.get('address').setValue(this.company.address);
        this.service.getCustomersByCompId(this.company.id).subscribe((resp) => {
          this.selectedCustomerList = toCustomers(resp.customers);
        }, () => {

        });
      });
    } else {
      this.companyForm.get('name').setValue(this.company.name);
      this.companyForm.get('name').disable();
      this.companyForm.get('founded').setValue(this.company.founded);
      this.companyForm.get('address').setValue(this.company.address);
      this.service.getCustomersByCompId(this.company.id).subscribe((resp) => {
        this.selectedCustomerList = toCustomers(resp.customers);
      }, () => {

      });
    }


    this.availableCustomerList = this.customerService.getAll();

    if (this.availableCustomerList.length === 0) {
      this.customerService.refreshAll().subscribe(() => {
        this.availableCustomerList = this.customerService.getAll();
      });
    }


  }
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
  ownerChanged($event) {
    if ($event.length === 0) {
      this.ownerForm.get('owners').setValue(null);
    } else {
      this.ownerForm.get('owners').setValue(true);
    }
    this.selectedCustomerList = $event;
  }
  navigateToParent(): void {
    this.router.navigate(['../../../'], { relativeTo: this.route });
  }
  onSaveOwners() {
    const ids = this.selectedCustomerList.map(p => p.id);
    this.service.assignOwners(ids, this.company.id).subscribe((resp) => {
      this.notify.successMessage('Sikeres mentés.');
    });
  }
  onSave() {
    const request: ModifyCompanyRequest = {
      company: {
        id: this.company.id,
        name: this.companyForm.get('name').value,
        founded: this.companyForm.get('founded').value,
        address: this.companyForm.get('address').value,
      }
    };
    this.service.modify(request).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Sikeres módosítás');
      } else {
        this.notify.error('Sikertelen módosítás.');
      }
    }, (err) => {
      this.notify.error('Sikertelen módosítás.');
    });
  }
}
