import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { regexConstants } from 'src/app/common-base/regex-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {

  companyForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    founded: new FormControl(null, [Validators.required]),
    address: new FormControl('', [Validators.required])
  });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CompanyService,
    private notify: NotificationService,
  ) { }

  ngOnInit(): void {
  }

  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  onSave() {
    const name = this.companyForm.get('name').value;
    const founded = this.companyForm.get('founded').value;
    const address = this.companyForm.get('address').value;
    this.service.create({ name, founded, address }).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Cég sikeresen létrehozva');
        this.navigateToParent();
      } else {
        this.notify.error('Cég létrehozása sikertelen');
      }
    }, (err) => {
      this.notify.error('Cég létrehozása sikertelen. Létező cégnevet adott meg.');

    });
  }

}
