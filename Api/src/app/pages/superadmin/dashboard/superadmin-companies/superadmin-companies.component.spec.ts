import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperadminCompaniesComponent } from './superadmin-companies.component';

describe('SuperadminCompaniesComponent', () => {
  let component: SuperadminCompaniesComponent;
  let fixture: ComponentFixture<SuperadminCompaniesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperadminCompaniesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperadminCompaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
