import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { SuperadminRoutingModule } from './superadmin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SuperadminLayoutComponent } from './superadmin-layout/superadmin-layout.component';
import { SuperadminCompaniesComponent } from './dashboard/superadmin-companies/superadmin-companies.component';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { CreateCompanyComponent } from './dashboard/superadmin-companies/create-company/create-company.component';
import { SuperadminStoresComponent } from './dashboard/superadmin-stores/superadmin-stores.component';
import { CreateStoreComponent } from './dashboard/superadmin-stores/create-store/create-store.component';
import { CompanyDetailsComponent } from './dashboard/superadmin-companies/company-details/company-details.component';
import { SuperadminCustomersComponent } from './dashboard/superadmin-customers/superadmin-customers.component';
import { ModifyStoreComponent } from './dashboard/superadmin-stores/modify-store/modify-store.component';
import { AssignAccountsComponent } from './dashboard/superadmin-stores/assign-accounts/assign-accounts.component';
import { CreateCustomerComponent } from './dashboard/superadmin-customers/create-customer/create-customer.component';
import { EditCustomerComponent } from './dashboard/superadmin-customers/edit-customer/edit-customer.component';



@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [DashboardComponent, SuperadminLayoutComponent, SuperadminCompaniesComponent, CreateCompanyComponent, SuperadminStoresComponent, CreateStoreComponent, CompanyDetailsComponent, SuperadminCustomersComponent, ModifyStoreComponent, AssignAccountsComponent, CreateCustomerComponent, EditCustomerComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SuperadminRoutingModule,
    CommonBaseModule
  ],
  exports: [SuperadminStoresComponent]
})
export class SuperadminModule { }
