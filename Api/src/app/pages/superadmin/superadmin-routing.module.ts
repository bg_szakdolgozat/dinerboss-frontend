import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompanyDetailsComponent } from './dashboard/superadmin-companies/company-details/company-details.component';
import { CreateCompanyComponent } from './dashboard/superadmin-companies/create-company/create-company.component';
import { CreateCustomerComponent } from './dashboard/superadmin-customers/create-customer/create-customer.component';
import { EditCustomerComponent } from './dashboard/superadmin-customers/edit-customer/edit-customer.component';
import { CreateStoreComponent } from './dashboard/superadmin-stores/create-store/create-store.component';
import { ModifyStoreComponent } from './dashboard/superadmin-stores/modify-store/modify-store.component';
const routes: Routes = [
    { path: '', component: DashboardComponent },
    { path: 'create', component: CreateCompanyComponent },
    { path: 'create-store', component: CreateStoreComponent },
    { path: 'create-customer', component: CreateCustomerComponent },
    { path: 'edit-customer/:id', component: EditCustomerComponent },
    { path: 'company/:id/details', component: CompanyDetailsComponent },
    { path: 'modify-store/:id', component: ModifyStoreComponent }


];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SuperadminRoutingModule { }
