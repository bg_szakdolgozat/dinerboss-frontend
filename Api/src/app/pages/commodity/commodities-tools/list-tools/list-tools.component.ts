import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { Tool } from 'src/app/services/tool/tool.model';
import { ToolService } from 'src/app/services/tool/tool.service';
import { ToolState } from 'src/app/services/tool/tool.store';
import { PageCommoditySearch, PageStoreSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-list-tools',
  templateUrl: './list-tools.component.html',
  styleUrls: ['./list-tools.component.css']
})
export class ListToolsComponent extends BaseList<Tool, ToolState, PageCommoditySearch> {
  searchValueForm: FormControl = new FormControl();
  id: string;
  storeIds: number[];
  compIds;
  constructor(
    private service: ToolService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private companyService: CompanyService
  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'id', 'name', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);
  }
  initDataSource(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);

    this.sort.direction = 'asc';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;

    if (ids.length === 0) {
      const storeIds = this.currentUserService.getStores().map(p => p.id);
      this.companyService.getByStoreIds({ storeIds }).subscribe((resp) => {
        this.compIds = resp.companies.map(p => p.id);
        this.loadDataSource({ compIds: this.compIds });

      });
    } else {
      this.loadDataSource();
    }
  }
  searchParams(): PageStoreSearch {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    if (ids.length > 0) {
      if (this.searchValueForm.value) {
        return {
          compIds: ids,
          value: this.searchValueForm.value
        };
      }
      return {
        compIds: ids,
      };
    } else {
      return {
        value: this.searchValueForm.value !== undefined ? this.searchValueForm.value : undefined,
        compIds: this.compIds
      };
    }

  }
  // tslint:disable-next-line:typedef
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
  onCreate() {
    this.router.navigate(['create-tool'], { relativeTo: this.route });
  }
  isOwner() {
    return this.currentUserService.isOwner();
  }
  onDelete(row) {
    const deleteTitle = 'Eszköz törlése';
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '500px',
      height: '200px',
      disableClose: true,
      data: deleteTitle
    });
    const dialogSub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(row.id).subscribe((resp) => {
          this.notify.successMessage('Sikeres törlés.');
          this.loadDataSource();
        });
      }
    }, () => {
      this.notify.error('Törlés sikertelen');

    });
  }
  onEdit(row: Tool) {
    this.router.navigate([row.id, 'modify-tool'], { relativeTo: this.route });

  }
}
