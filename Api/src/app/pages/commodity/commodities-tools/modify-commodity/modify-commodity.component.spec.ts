import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyCommodityComponent } from './modify-commodity.component';

describe('ModifyCommodityComponent', () => {
  let component: ModifyCommodityComponent;
  let fixture: ComponentFixture<ModifyCommodityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyCommodityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyCommodityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
