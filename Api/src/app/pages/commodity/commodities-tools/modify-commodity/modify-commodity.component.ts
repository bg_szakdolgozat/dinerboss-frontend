import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { Commodity } from 'src/app/services/commodities/commodity.model';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { CommodityDTO, CreateCommodityDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-modify-commodity',
  templateUrl: './modify-commodity.component.html',
  styleUrls: ['./modify-commodity.component.css']
})
export class ModifyCommodityComponent implements OnInit {
  companies: Company[];
  commodityForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    purchasePrice: new FormControl(null),
    unit: new FormControl(null),
    warranty: new FormControl(null),
    company: new FormControl('', [Validators.required])
  });
  id: number;
  commodity: Commodity;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CommodityService,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private companyService: CompanyService
  ) { }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.commodity = this.service.getById(this.id.toString());
    if (this.commodity === undefined) {
      this.service.refreshAll().subscribe(() => {
        this.commodity = this.service.getById(this.id.toString());
        this.fillForm();
        this.companyService.refreshAll().subscribe(() => {
          this.commodityForm.get('company').setValue(this.companyService.getById(this.commodity.compId.toString()));
        });
      });
    } else {
      this.fillForm();
    }
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.companyService.refreshByCompIds(ids).subscribe(() => {
      this.companies = this.companyService.getAll();
    });


  }
  fillForm() {
    this.commodityForm.get('name').setValue(this.commodity.name);
    this.commodityForm.get('purchasePrice').setValue(this.commodity.purchasePrice);
    this.commodityForm.get('unit').setValue(this.commodity.unit);
    this.commodityForm.get('warranty').setValue(this.commodity.warranty);
    this.commodityForm.get('company').setValue(this.companyService.getById(this.commodity.compId.toString()));
  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }



  onModify() {
    const dto: CommodityDTO = {
      id: this.id,
      name: this.commodityForm.get('name').value,
      purchasePrice: this.commodityForm.get('purchasePrice').value ? this.commodityForm.get('purchasePrice').value : undefined,
      unit: this.commodityForm.get('unit').value ? this.commodityForm.get('unit').value : undefined,
      warranty: this.commodityForm.get('warranty').value ? this.commodityForm.get('warranty').value : undefined,
      compId: this.commodityForm.get('company').value.id
    };
    this.service.modify(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Alapanyag sikeresen módosítva');
        this.navigateToParent();
      } else {
        this.notify.error('Alapanyag módosítása sikertelen');
      }
    }, (err) => {
      this.notify.error('Alapanyag módosítása sikertelen.');

    });
  }
}
