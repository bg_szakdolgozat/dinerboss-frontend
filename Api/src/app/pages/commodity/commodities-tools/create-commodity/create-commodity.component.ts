import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { CreateCommodityDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-create-commodity',
  templateUrl: './create-commodity.component.html',
  styleUrls: ['./create-commodity.component.css']
})
export class CreateCommodityComponent implements OnInit {
  companies: Company[];
  commodityForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    purchasePrice: new FormControl(null),
    unit: new FormControl(null),
    warranty: new FormControl(null),
    company: new FormControl('', [Validators.required])
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: CommodityService,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private companyService: CompanyService
  ) { }

  ngOnInit(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.companyService.refreshByCompIds(ids).subscribe(() => {
      this.companies = this.companyService.getAll();
    });

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }



  onCreate() {
    const dto: CreateCommodityDTO = {
      name: this.commodityForm.get('name').value,
      purchasePrice: this.commodityForm.get('purchasePrice').value ? this.commodityForm.get('purchasePrice').value : undefined,
      unit: this.commodityForm.get('unit').value ? this.commodityForm.get('unit').value : undefined,
      warranty: this.commodityForm.get('warranty').value ? this.commodityForm.get('warranty').value : undefined,
      compId: this.commodityForm.get('company').value.id
    };
    this.service.create(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Alapanyag sikeresen létrehozva');
        this.navigateToParent();
      } else {
        this.notify.error('Alapanyag létrehozása sikertelen');
      }
    }, (err) => {
      this.notify.error('Alapanyag létrehozása sikertelen.');

    });
  }
}
