import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { regexConstants } from 'src/app/common-base/regex-util';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { Company } from 'src/app/services/company/company.model';
import { CompanyService } from 'src/app/services/company/company.service';
import { ToolService } from 'src/app/services/tool/tool.service';
import { CreateCommodityDTO } from 'src/app/shared/openapi';

@Component({
  selector: 'app-create-tool',
  templateUrl: './create-tool.component.html',
  styleUrls: ['./create-tool.component.css']
})
export class CreateToolComponent implements OnInit {
  companies: Company[];
  commodityForm = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.pattern(regexConstants.name)]),
    company: new FormControl('', [Validators.required])
  });
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: ToolService,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private companyService: CompanyService
  ) { }

  ngOnInit(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.companyService.refreshByCompIds(ids).subscribe(() => {
      this.companies = this.companyService.getAll();
    });

  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }



  onCreate() {
    const dto: CreateCommodityDTO = {
      name: this.commodityForm.get('name').value,
      compId: this.commodityForm.get('company').value.id
    };
    this.service.create(dto).subscribe((resp) => {
      if (resp.rc === 0) {
        this.notify.successMessage('Eszköz sikeresen létrehozva');
        this.navigateToParent();
      } else {
        this.notify.error('Eszköz létrehozása sikertelen');
      }
    }, (err) => {
      this.notify.error('Eszköz létrehozása sikertelen.');

    });
  }
}
