import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommoditiesToolsComponent } from './commodities-tools.component';

describe('CommoditiesToolsComponent', () => {
  let component: CommoditiesToolsComponent;
  let fixture: ComponentFixture<CommoditiesToolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommoditiesToolsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommoditiesToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
