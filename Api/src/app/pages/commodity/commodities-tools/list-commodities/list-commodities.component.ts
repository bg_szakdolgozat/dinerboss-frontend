import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmDeleteComponent } from 'src/app/common-base/confirm-delete/confirm-delete.component';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { Commodity } from 'src/app/services/commodities/commodity.model';
import { CommodityService } from 'src/app/services/commodities/commodity.service';
import { CommodityState } from 'src/app/services/commodities/commodity.store';
import { BaseList } from 'src/app/services/common/datasource-template/base-list';
import { DateUtil } from 'src/app/services/common/date-util';
import { NotificationService } from 'src/app/services/common/notification.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { Store } from 'src/app/services/store/store.model';
import { StoreService } from 'src/app/services/store/store.service';
import { PageCommoditySearch, PageStoreSearch } from 'src/app/shared/openapi';

@Component({
  selector: 'app-list-commodities',
  templateUrl: './list-commodities.component.html',
  styleUrls: ['./list-commodities.component.css']
})
export class ListCommoditiesComponent extends BaseList<Commodity, CommodityState, PageCommoditySearch> {
  searchValueForm: FormControl = new FormControl();
  storeForm: FormControl = new FormControl();

  id: string;
  storeIds: number[];
  store: Store;
  compIds;
  constructor(
    private service: CommodityService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notify: NotificationService,
    private currentUserService: CurrentUserService,
    private storeService: StoreService,
    private companyService: CompanyService
  ) {
    super(service, router);
  }

  initColumns(): string[] {
    return ['select', 'id', 'name', 'purchasePrice', 'unit', 'warranty', 'company', 'actions'];
  }
  initColumnsOnDevice(): string[] {
    return this.initColumns();
  }
  initFilter(): void {
    this.searchControlInit(this.searchValueForm);

  }
  initDataSource(): void {
    const ids = this.currentUserService.getCompanies().map(p => p.id);

    /* this.service.getStoresByCompIds(ids).subscribe(() => {
      const stores = this.service.getAll();
      const storeIds = stores.map(s => s.id);
      this.storeIds = storeIds;
    }); */
    this.sort.direction = 'asc';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    if (ids.length === 0) {
      const storeIds = this.currentUserService.getStores().map(p => p.id);
      this.companyService.getByStoreIds({ storeIds }).subscribe((resp) => {
        this.compIds = resp.companies.map(p => p.id);
        this.loadDataSource({ compIds: this.compIds });

      });
    } else {
      this.loadDataSource();
    }

  }

  searchParams(): PageStoreSearch {
    const ids = this.currentUserService.getCompanies().map(p => p.id);



    if (ids.length > 0) {
      if (this.searchValueForm.value) {
        return {
          compIds: ids,
          value: this.searchValueForm.value
        };
      }
      return {
        compIds: ids,
      };
    } else if (ids.length === 0) {

      return {
        value: this.searchValueForm.value !== undefined ? this.searchValueForm.value : undefined,
        compIds: this.compIds
      };



    }
    return {
    };
  }
  // tslint:disable-next-line:typedef
  localizeDate(date: Date) {
    return DateUtil.getDate(date);
  }
  isOwner() {
    return this.currentUserService.isOwner();
  }
  onCreate() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
  onDelete(row) {
    const deleteTitle = 'Alapanyag törlése';
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '500px',
      height: '200px',
      disableClose: true,
      data: deleteTitle
    });
    const dialogSub = dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(row.id).subscribe((resp) => {
          this.notify.successMessage('Sikeres törlés.');
          this.loadDataSource();
        });
      }
    }, () => {
      this.notify.error('Törlés sikertelen');

    });
  }
  onEdit(row: Commodity) {
    this.router.navigate([row.id, 'modify-commodity'], { relativeTo: this.route });

  }
}
