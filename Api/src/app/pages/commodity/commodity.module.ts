import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommoditiesToolsComponent } from './commodities-tools/commodities-tools.component';
import { CommoditiesRoutingModule } from './commodity-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ListCommoditiesComponent } from './commodities-tools/list-commodities/list-commodities.component';
import { ListToolsComponent } from './commodities-tools/list-tools/list-tools.component';
import { CreateCommodityComponent } from './commodities-tools/create-commodity/create-commodity.component';
import { ModifyCommodityComponent } from './commodities-tools/modify-commodity/modify-commodity.component';
import { CreateToolComponent } from './commodities-tools/create-tool/create-tool.component';



@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [CommoditiesToolsComponent, ListCommoditiesComponent, ListToolsComponent, CreateCommodityComponent, ModifyCommodityComponent, CreateToolComponent],
  imports: [
    CommonModule,
    CommoditiesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CommonBaseModule
  ]
})
export class CommodityModule { }
