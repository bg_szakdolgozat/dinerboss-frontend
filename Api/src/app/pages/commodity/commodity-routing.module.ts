import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommoditiesToolsComponent } from './commodities-tools/commodities-tools.component';
import { CreateCommodityComponent } from './commodities-tools/create-commodity/create-commodity.component';
import { CreateToolComponent } from './commodities-tools/create-tool/create-tool.component';
import { ModifyCommodityComponent } from './commodities-tools/modify-commodity/modify-commodity.component';


const routes: Routes = [
    { path: '', component: CommoditiesToolsComponent },
    { path: 'create', component: CreateCommodityComponent },
    { path: 'create-tool', component: CreateToolComponent },
    { path: ':id/modify-commodity', component: ModifyCommodityComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CommoditiesRoutingModule { }
