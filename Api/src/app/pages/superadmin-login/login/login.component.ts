import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSuperadminService } from 'src/app/services/auth/current-superadmin.service';
import { NotificationService } from 'src/app/services/common/notification.service';
import { SuperadminService } from 'src/app/services/domain/league/superadmin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  constructor(
    private elementRef: ElementRef,
    private currentSuperadminService: CurrentSuperadminService,
    private service: SuperadminService,
    private router: Router,
    private route: ActivatedRoute,
    private notify: NotificationService

  ) { }
  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundImage = 'url(\'assets/background.jpg\'';
  }
  doLogin(): void {

    const email = this.loginForm.get('email').value;
    const pw = this.loginForm.get('password').value;
    this.service.login(email, pw).subscribe((res) => {
      if (res != null && res.rc === 0) {
        // tslint:disable-next-line:max-line-length
        this.currentSuperadminService.create({ email: res.admin.email, firstName: res.admin.firstName, lastName: res.admin.lastName, role: 1 });

        this.notify.successMessage('Sikeres bejelentkezés');
        this.router.navigate(['/superadmin/'], { relativeTo: this.route });
      }

    }, (err) => {
      this.notify.error('Sikertelen bejelentkezés');

    });

  }
}
