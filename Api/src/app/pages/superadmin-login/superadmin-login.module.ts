import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { SuperadminLoginRoutingModule } from './superadmin-login-routing.module';
import { CurrentSuperadminService } from 'src/app/services/auth/current-superadmin.service';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SuperadminLoginRoutingModule
  ],
  providers: [CurrentSuperadminService]
})
export class SuperadminLoginModule { }
