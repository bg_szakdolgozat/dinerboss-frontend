import { OnChanges, SimpleChanges } from '@angular/core';
import { ChangeDetectorRef, Component, HostListener, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { now } from 'moment';
import { SimpleCard } from 'src/app/common-base/simple-card/simple-card.component';
import { CurrentUserService } from 'src/app/services/auth/current-user.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { StoreService } from 'src/app/services/store/store.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit, OnChanges {
  storeIncomeData;
  view: any[] = [window.innerWidth - 350, 350];
  companies: number;
  stores: number;
  customers: number;
  storages: number;
  storelist;
  storeIds;
  statForm = new FormGroup({
    store: new FormControl(null),

  });
  constructor(
    private companyService: CompanyService,
    private changeDetectorRef: ChangeDetectorRef,
    private storeService: StoreService,
    private storageService: StorageService,
    private customerService: CustomerService,
    private currentUserService: CurrentUserService
  ) { }
  multi;
  allRegistered: SimpleCard;
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  legendTitle = '';
  colorScheme = {
    domain: ['#c59a6b', '#894e00', '#dd4b47']
  };
  legend = true;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  timeline = true;
  ngOnInit(): void {
    this.allRegistered = {
      sum: 2,
      proValue: 3,
      liteValue: 4
    };
    const ids = this.currentUserService.getCompanies().map(p => p.id);
    this.companyService.getByCompIds(ids).subscribe(resp => {
      this.companies = resp.totalCount;
      this.storeService.getStoresByCmpIds(ids).subscribe(resp2 => {
        this.storelist = resp2.data;
        this.stores = resp2.totalCount;
        this.storeIds = resp2.data.map(p => p.id);
        this.fillStoreIncomeData(7);

        this.storageService.getAllByStore(resp2.data.map(p => p.id)).subscribe(resp3 => {
          this.storages = resp3.stores.length;
          this.customerService.getByStoreIds(resp2.data.map(p => p.id)).subscribe(resp4 => {
            this.customers = resp4.totalCount;
          });
        });
      });
    });
  }

  fillStoreIncomeData(days) {
    const data = new Array<ChartSeries>();
    this.storeIncomeData = new Array<ChartSeries>();
    this.companyService.getStoreIncomes({ storeIds: this.storeIds, days: 14 }).subscribe((resp) => {
      this.storeIncomeData = resp.data;
      this.statForm.get('store').setValue(1);
      this.companyService.getStoreReports({ storeIds: [this.statForm.get('store').value], days: 14 }).subscribe((resp2) => {
        this.multi = resp2.data;
      });
    });

  }
  ngOnChanges(changes: SimpleChanges): void {
  }
  onSelect($event) {
  }
  displayName(data): string {
    if (!data) { return ''; }
    return data.name;
  }
  onActivate(data): void {
  }
  changeFunction() {
    console.log('asd');
    console.log(this.statForm.get('store').value);

    this.companyService.getStoreReports({ storeIds: [this.statForm.get('store').value.id], days: 14 }).subscribe((resp2) => {
      this.multi = resp2.data;
    });
  }
  onDeactivate(data): void {
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // tslint:disable-next-line:no-unused-expression
    this.view = [window.innerWidth - 350, 350];
    this.changeDetectorRef.detectChanges();
  }
}
export interface ChartSeries {
  name: string;
  series: ChartDatas[];
}
export interface ChartDatas {
  name: string;
  value: number;
}
