import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsComponent } from './stats/stats.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonBaseModule } from 'src/app/common-base/common-base.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { CheckoutRoutingModule } from '../checkout/checkout-routing.module';
import { StatisticsRoutingModule } from './statistics-routing.module';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { NgxChartsModule } from '@swimlane/ngx-charts';



@NgModule({
  declarations: [StatsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    StatisticsRoutingModule,
    CommonBaseModule,
    NgxChartsModule,
    MatButtonToggleModule
  ]
})
export class StatisticsModule { }
