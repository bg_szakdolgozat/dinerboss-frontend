import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './pages/admin/admin-layout/admin-layout.component';
import { AdminGuard } from './pages/admin/admin.guard';
import { ReportGuard } from './pages/report/report.guard';
import { CreateStoreComponent } from './pages/superadmin/dashboard/superadmin-stores/create-store/create-store.component';
import { SuperadminLayoutComponent } from './pages/superadmin/superadmin-layout/superadmin-layout.component';
import { SuperadminGuard } from './pages/superadmin/superadmin.guard';

const routes: Routes = [
  {
    path: '', loadChildren: () => import('./pages/landing-page/landing-page.module').then(m => m.LandingPageModule),
  },
  {
    // tslint:disable-next-line:max-line-length
    path: 'loginsuperadmin', loadChildren: () => import('./pages/superadmin-login/superadmin-login.module').then(m => m.SuperadminLoginModule),
  },
  {
    // tslint:disable-next-line:max-line-length
    path: 'loginuser', loadChildren: () => import('./pages/user-login/user-login.module').then(m => m.UserLoginModule),
  },
  {
    path: 'superadmin', component: SuperadminLayoutComponent, children: [
      {
        // tslint:disable-next-line:max-line-length
        path: '', loadChildren: () => import('./pages/superadmin/superadmin.module').then(m => m.SuperadminModule), canActivate: [SuperadminGuard]
      },
      { path: 'create-store', component: CreateStoreComponent },


    ]
  },
  {
    path: 'admin', component: AdminLayoutComponent, children: [
      {
        // tslint:disable-next-line:max-line-length
        path: '', loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule)

      },
      {
        // tslint:disable-next-line:max-line-length
        path: 'stores', loadChildren: () => import('./pages/stores/stores.module').then(m => m.StoresModule)
      },
      {
        // tslint:disable-next-line:max-line-length
        path: 'customers', loadChildren: () => import('./pages/customers/customers.module').then(m => m.CustomersModule)
      },
      {
        // tslint:disable-next-line:max-line-length
        path: 'commodities', loadChildren: () => import('./pages/commodity/commodity.module').then(m => m.CommodityModule)
      },
      {
        // tslint:disable-next-line:max-line-length
        path: 'storages', loadChildren: () => import('./pages/storages/storages.module').then(m => m.StoragesModule)
      },
      {
        // tslint:disable-next-line:max-line-length
        path: 'checkouts', loadChildren: () => import('./pages/checkout/checkout.module').then(m => m.CheckoutModule)
      },
      {
        // tslint:disable-next-line:max-line-length
        path: 'report', loadChildren: () => import('./pages/report/report.module').then(m => m.ReportModule), canActivate: [ReportGuard]
      },
      {
        // tslint:disable-next-line:max-line-length
        path: 'statistics', loadChildren: () => import('./pages/statistics/statistics.module').then(m => m.StatisticsModule), canActivate: [ReportGuard]
      }
    ], canActivate: [AdminGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
