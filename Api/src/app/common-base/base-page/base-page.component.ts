import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BreadcrumbItem } from '../breadcrumb/breadcrumb.component';

@Component({
  selector: 'app-base-page',
  templateUrl: './base-page.component.html',
  styleUrls: ['./base-page.component.scss']
})
export class BasePageComponent implements OnInit, OnChanges {
  @Input() transparentContainer = false;
  @Input() breadcrumbItemList: BreadcrumbItem[];
  @Input() allowNavigateBack = false;
  @Output() navigateBack: EventEmitter<void> = new EventEmitter();

  constructor(
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  navigateToParent() {
    this.navigateBack.emit();
  }

}
