import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-simple-card',
  templateUrl: './simple-card.component.html',
  styleUrls: ['./simple-card.component.css']
})
export class SimpleCardComponent implements OnInit {
  @Input() title: string;
  @Input() comment: string;
  @Input() value: SimpleCard;

  constructor() { }

  ngOnInit(): void {
  }

}
export interface SimpleCard {
  sum: number;
  liteValue: number;
  proValue: number;
}
