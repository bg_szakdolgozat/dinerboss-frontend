import { Component, OnInit, Input, OnChanges, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Nameable } from 'src/app/services/common/base-entity';
import { removeDiacritics } from 'src/app/services/common/remove-diacrictis';

@Component({
  selector: 'app-base-picklist',
  templateUrl: './base-picklist.component.html',
  styleUrls: ['./base-picklist.component.scss']
})
export class BasePicklistComponent<T extends Nameable> implements OnInit, OnChanges {

  availableItemsFilter = new FormControl('');
  selectedItemsFilter = new FormControl('');

  availableItemsOriginal: T[] = [];
  selectedItemsOriginal: T[] = [];

  subManager = new Subscription();
  translationKeys = {};

  @Input() translationBaseName: string;
  @Input() autoSelect = false;
  @Input() required = false;
  @Input() selectedItems: T[] = [];
  @Input() availableItems: T[] = [];

  @Output() changedItemsEvent: EventEmitter<T[]> = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
    this.translationKeys['available.title'] = `${this.translationBaseName}.picklist.available.title`;
    this.translationKeys['available.button.title'] = `${this.translationBaseName}.picklist.available.button.title`;
    this.translationKeys['selected.title'] = `${this.translationBaseName}.picklist.selected.title`;
    this.translationKeys['selected.button.title'] = `${this.translationBaseName}.picklist.selected.button.title`;
    this.refreshItems();
    this.initFilters();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.refreshItems();
  }

  refreshItems() {
    const rawAvailableItems = Array.from(this.availableItems || []);
    if (this.selectedItems) {
      this.availableItems = rawAvailableItems.filter(available => {
        return !this.selectedItems.some(sel => sel.email === available.email);
      });
    }

    this.availableItemsOriginal = Array.from(this.availableItems);
    this.selectedItemsOriginal = Array.from(this.selectedItems || []);

    if (this.autoSelect) {
      this.selectAll();
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.emitChanges();
  }

  emitChanges() {
    this.availableItemsOriginal = Array.from(this.availableItems);
    this.selectedItemsOriginal = Array.from(this.selectedItems);
    this.changedItemsEvent.emit(this.selectedItems);
  }

  initFilters() {
    const filterSub1 = this.availableItemsFilter.valueChanges.pipe(
      debounceTime(250),
      distinctUntilChanged()
    ).subscribe(value => {
      this.availableItems = this.availableItemsOriginal.filter(v => {
        return removeDiacritics(v.email).toLowerCase().includes(removeDiacritics(value.toLowerCase()));
      });
    });
    this.subManager.add(filterSub1);
    const filterSub2 = this.selectedItemsFilter.valueChanges.pipe(
      debounceTime(250),
      distinctUntilChanged()
    ).subscribe(value => {
      this.selectedItems = this.selectedItemsOriginal.filter(v => {
        return removeDiacritics(v.email).toLowerCase().includes(removeDiacritics(value.toLowerCase()));
      });
    });
    this.subManager.add(filterSub2);
  }

  selectAll() {
    this.availableItems.forEach(item => this.selectedItems.push(item));
    this.availableItems = Array.from([]);
    this.emitChanges();
  }

  selectNone() {
    this.selectedItems.forEach(item => this.availableItems.push(item));
    this.selectedItems = Array.from([]);
    this.emitChanges();
  }
}
