import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.css']
})
export class ConfirmDeleteComponent implements OnInit {
  deleteTitle = 'Delete';
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string
  ) {
  }

  ngOnInit(): void {
    this.deleteTitle = this.data;
  }
}
