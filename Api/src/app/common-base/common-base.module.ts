import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasePageComponent } from './base-page/base-page.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material/material.module';
import { CtPageContentDirective } from './base-page/page-content.directive';
import { CtPageHeaderDirective } from './base-page/page-header.directive';
import { CtPageFooterDirective } from './base-page/page-footer.directive';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { BasePicklistComponent } from './base-picklist/base-picklist.component';
// tslint:disable-next-line:max-line-length
import { IgxAvatarModule, IgxFilterModule, IgxIconModule, IgxListModule, IgxInputGroupModule, IgxButtonGroupModule } from 'igniteui-angular';
import { SimpleCardComponent } from './simple-card/simple-card.component';



@NgModule({
  declarations: [BasePageComponent, BreadcrumbComponent,
    CtPageHeaderDirective,
    CtPageContentDirective,
    CtPageFooterDirective,
    ConfirmDeleteComponent,
    BasePicklistComponent,
    SimpleCardComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule, RouterModule,

  ], exports: [BasePageComponent, CtPageHeaderDirective,
    CtPageContentDirective,
    CtPageFooterDirective,
    BasePicklistComponent,
    SimpleCardComponent]
})
export class CommonBaseModule { }
