export const regexConstants = {
  nationalChars: '^[\\p{L}0-9]*$',
  alphaNumericChars: '^[A-Za-z0-9]*$',
  smallAlphanumericChars: '^[a-z0-9\s]+$',
  phoneNumber: '\\+[0-9]{11,30}',
  adminUserName: '^[A-Za-z0-9._\s]+$',
  nationalCharsWithSpace: '^[\\p{L}.\\-0-9 ]*$',
  nullableNationalCharsWithSpace: '^[\\p{L}.\\-0-9 ]*$',
  idChars: /^[A-Za-z0-9_.\\-]*$/,
  domainPattern: '^[A-Za-z0-9_]+([\\-.:/][A-Za-z0-9_]+)*$',
  personNamePart: '^\\p{L}+[\\p{L}\\p{Zs}\\p{Pd}\\p{Pc}\\p{Nd}.\']*$',
  descriptionPattern: '^[A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüű\\p{L}\\.\\+&amp;_\'\\-\\(\\),/! ]+$',
  emailFirstPart: '[a-z0-9_=]+([-+.\'][a-z0-9_=]+)*',
  emailPattern: '[A-Za-z0-9_=]+([-+.\'][A-Za-z0-9_=]+)*@[A-Za-z0-9_]+([-.][A-Za-z0-9_]+)*\\.[A-Za-z0-9_]+([-.][A-Za-z0-9_]+)*',
  orgNamePattern: /[A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüű\\p{L}.+&amp;_'\-(),/! ]+/,
  hardwareIdPattern: /([a-fA-F0-9]{2}[:-]){5}([a-fA-F0-9]{2})/,
  alphaNumericWithSpecials: '^[a-zA-Z0-9._$=*+/,@-\s]+$',
  name: '[A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüű\\p{L}][A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüűp{L}.\\- ]*',
  parameterName: {
    pattern: '^[A-Za-z0-9]*$',
    minLength: 1,
    maxLength: 255
  },
  parameterValue: {
    pattern: '^[A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüű\\p{L} \\.\\-;:,!+/=]*$',
    minLength: 0,
    maxLength: 8096
  }
};
